load('[2014-07-23][12h04]tsc_fields_logs.mat');

demoTime = [  13.043,46.243;
             5*60+20,5*60+50;    
            10*60+40,11*60+10];

rehearsal_init = [45,5*60+50,11*60+10];
rehearsal_end = [3*60+35,8*60+45,14*60+5];

addpath('D:\Dropbox\WorkDir\RP-Sequences\ML_sequencesToolbox\');

for i=1:3,

meandursDemo = CalculateMeanDurBetweenSubTasks( tsc_populations,demoTime(i,1),demoTime(i,2));

meandursReh = CalculateMeanDurBetweenSubTasks( tsc_populations,rehearsal_init(i),rehearsal_end(i) );

figure;
bar([meandursReh;meandursDemo]');


end
TSVisualizer(tsc_populations);