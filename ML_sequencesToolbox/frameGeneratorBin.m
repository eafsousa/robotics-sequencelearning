function [ output_args ] = frameGeneratorBin(varargin)% tsc, data_struct, tstart, tend ,lambda_pa,lambda_pr,labels,op,FPS)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

tsc = [];
data_struct = [];
tstart = 0;
tend = 0;
lambda_pa = nan;
lambda_pr = nan;
fps = [];

maxylimpr = 5;


blockLen = 1000;

videofilename = 'newAnimation.avi';

argin = length(varargin);

fieldsize = 100;

quality = 75;

flag_generateSTMData = 0;

for n = 1:1:argin
    arg = varargin{n};
    if isa(arg,'char')
        switch lower(arg)
            case '-tsc'
                %Gets the tsc
                tsc = varargin{n+1};
            case '-past_u_file'
                past_u_file = varargin{n+1};
            case '-past_s_file'
                past_s_file = varargin{n+1};
            case '-present_s_file'
                pres_s_file = varargin{n+1};
            case '-present_u_file'
                pres_u_file = varargin{n+1};
            case '-stm_u_file'
                stm_u_file = varargin{n+1};
            case '-stm_s_file'
                stm_s_file = varargin{n+1};
            case '-tstart'
                tstart = varargin{n+1};
            case '-tend'
                tend = varargin{n+1};
            case '-lambda_pa'
                lambda_pa = varargin{n+1};
            case '-lambda_pr'
                lambda_pr = varargin{n+1};
            case '-labels'
                labels = varargin{n+1};
            case '-fps'
                fps = varargin{n+1};
            case '-gif'
                op = 'gif';
            case '-vid'
                op = 'vid';
            case '-outputfile'
                videofilename = varargin{n+1};
            case 'fieldsize'
                fieldsize = varargin{n+1};
            case '-maxylimpr'
                maxylimpr = varargin{n+1};
            case '-quality'
                quality = varargin{n+1};
            case 'generatestmdata'
                flag_generateSTMData = 1;
        end
    end
end

%assert(~isempty(data_struct), 'No data structure containing fields was passed');
assert(~isempty(fps),'Must define a value for fps');

% Calculates the time for each frame
T = 1/fps;

if ~isempty(tsc)
    [~ ,indexStart] = min(abs(tsc.s_past.Time-tstart));
    if tend~=0,
        [~ ,indexEnd] = min(abs(tsc.s_past.Time-tend));
    else
        indexEnd = length(tsc.s_past.Time);
    end
end

len = indexEnd - indexStart;

blocksNumber = floor(len/blockLen);
n_last = indexEnd - blocksNumber*blockLen;

% Create and open the video object
writerObj = VideoWriter(videofilename);
writerObj.FrameRate = fps;
writerObj.Quality = quality;
open(writerObj);

fig = figure('visible','off');

labelLen = length(labels);

spreadBetLabels = fieldsize/labelLen;

vecLabel = spreadBetLabels/2:spreadBetLabels:fieldsize;

if ~flag_generateSTMData
    
    past_u_data_fid = fopen(past_u_file);
    past_s_data_fid = fopen(past_s_file);
    pres_u_data_fid = fopen(pres_u_file);
    pres_s_data_fid = fopen(pres_s_file);
    
    subplot(2,1,1);
    h1 = plot(zeros(1,fieldsize),'r','LineWidth',3);
    hold on
    h4 = plot(zeros(1,fieldsize),'b','LineWidth',2);
    plot(zeros(1,fieldsize),'k');
    if ~isnan(lambda_pa)
        plot(lambda_pa * ones(1,fieldsize),'k--');
    end
    ylim([-3 10]);
    ylabel(gca,'u^{pa}(x^{pa},t)');
    xlim([1 fieldsize]);
    xlabel(gca,'x^{pa}');
    set(gca,'Xtick',vecLabel,'XTickLabel',labels);
    title('Past Layer');
    
    subplot(2,1,2);
    h2 = plot(zeros(1,fieldsize),'r','LineWidth',3);
    hold on
    h3 = plot(zeros(1,fieldsize),'b','LineWidth',2);
    plot(zeros(1,fieldsize),'k');
    if ~isnan(lambda_pr)
        plot(lambda_pr * ones(1,fieldsize),'k--');
    end
    ylim([-5 maxylimpr]);
    ylabel(gca,'u^{pr}(x^{pr},t)');
    xlim([1 fieldsize]);
    set(gca,'Xtick',vecLabel,'XTickLabel',labels);
    xlabel(gca,'x^{pr}');
    title('Present Layer');
    
    %sets the size of the print to be equal to the figure size
    set(gcf,'PaperPositionMode','auto');
    
    fseek(past_u_data_fid, 8*fieldsize*(indexStart-1), 'bof');
    fseek(past_s_data_fid, 8*fieldsize*(indexStart-1), 'bof');
    fseek(pres_u_data_fid, 8*fieldsize*(indexStart-1), 'bof');
    fseek(pres_s_data_fid, 8*fieldsize*(indexStart-1), 'bof');
    
    
    %Take care of the first blocks
    
    for k = 1: blocksNumber
        
        u_past = fread(past_u_data_fid,[fieldsize,blockLen],'double');
        s_past = fread(past_s_data_fid,[fieldsize,blockLen],'double');
        u_pres = fread(pres_u_data_fid,[fieldsize,blockLen],'double');
        s_pres = fread(pres_s_data_fid,[fieldsize,blockLen],'double');
        
        in_u_past = interp2( 1:fieldsize , tsc.Time(indexStart+(k-1)*blockLen:indexStart+k*blockLen-1)' , [u_past]' , 1:fieldsize , [tsc.Time(indexStart+(k-1)*blockLen):T:tsc.Time(indexStart+k*blockLen)]')';
        in_s_past = interp2( 1:fieldsize , tsc.Time(indexStart+(k-1)*blockLen:indexStart+k*blockLen-1)' , [s_past]' , 1:fieldsize , [tsc.Time(indexStart+(k-1)*blockLen):T:tsc.Time(indexStart+k*blockLen)]')';
        in_u_pres = interp2( 1:fieldsize , tsc.Time(indexStart+(k-1)*blockLen:indexStart+k*blockLen-1)' , [u_pres]' , 1:fieldsize , [tsc.Time(indexStart+(k-1)*blockLen):T:tsc.Time(indexStart+k*blockLen)]')';
        in_s_pres = interp2( 1:fieldsize , tsc.Time(indexStart+(k-1)*blockLen:indexStart+k*blockLen-1)' , [s_pres]' , 1:fieldsize , [tsc.Time(indexStart+(k-1)*blockLen):T:tsc.Time(indexStart+k*blockLen)]')';
        
        
        [~,len_interp] = size(in_u_past);
        
        for i=1:len_interp
            
            %Updates the figure
            set(h1,'ydata',in_u_past(:,i));
            set(h4,'ydata',in_s_past(:,i));
            set(h2,'ydata',in_s_pres(:,i));
            set(h3,'ydata',in_u_pres(:,i));
            frame = getframe(fig);
            
            %If its a movie, writes the frame to video obj
            writeVideo(writerObj,frame);
        end
    end
    %Take care of the last (incomplete block)
    
    u_past = fread(past_u_data_fid,[fieldsize,n_last],'double');
    s_past = fread(past_s_data_fid,[fieldsize,n_last],'double');
    u_pres = fread(pres_u_data_fid,[fieldsize,n_last],'double');
    s_pres = fread(pres_s_data_fid,[fieldsize,n_last],'double');
    
    in_u_past = interp2( 1:fieldsize , tsc.Time(blocksNumber*blockLen+1:indexEnd)' , u_past' , 1:fieldsize , [tsc.Time(indexStart+blocksNumber*blockLen+1):T:tsc.Time(indexEnd)]')';
    in_s_past = interp2( 1:fieldsize , tsc.Time(blocksNumber*blockLen+1:indexEnd)' , s_past' , 1:fieldsize , [tsc.Time(indexStart+blocksNumber*blockLen+1):T:tsc.Time(indexEnd)]')';
    in_u_pres = interp2( 1:fieldsize , tsc.Time(blocksNumber*blockLen+1:indexEnd)' , u_pres' , 1:fieldsize , [tsc.Time(indexStart+blocksNumber*blockLen+1):T:tsc.Time(indexEnd)]')';
    in_s_pres = interp2( 1:fieldsize , tsc.Time(blocksNumber*blockLen+1:indexEnd)' , s_pres' , 1:fieldsize , [tsc.Time(indexStart+blocksNumber*blockLen+1):T:tsc.Time(indexEnd)]')';
    
    
    [~,len_interp] = size(in_u_past);
    
    
    for i=1:len_interp
        
        %Updates the figure
        set(h1,'ydata',in_u_past(:,i));
        set(h4,'ydata',in_s_past(:,i));
        set(h2,'ydata',in_s_pres(:,i));
        set(h3,'ydata',in_u_pres(:,i));
        frame = getframe(fig);
        
        %If its a movie, writes the frame to video obj
        writeVideo(writerObj,frame);
    end
    close(writerObj);
    
else

    stm_u_data_fid = fopen(stm_u_file);
    stm_s_data_fid = fopen(stm_s_file);

    h1 = plot(zeros(1,fieldsize),'r','LineWidth',3);
    hold on
    h2 = plot(zeros(1,fieldsize),'b','LineWidth',2);
    plot(zeros(1,fieldsize),'k');

    ylim([-3 25]);
    ylabel(gca,'u^{stm}(x^{stm},t)');
    xlim([1 fieldsize]);
    xlabel(gca,'x^{stm}');
    set(gca,'Xtick',vecLabel,'XTickLabel',labels);
    title('Short-Term Memory Layer');
     
    %sets the size of the print to be equal to the figure size
    set(gcf,'PaperPositionMode','auto');
        
    fseek(stm_u_data_fid, 8*fieldsize*(indexStart-1), 'bof');
    fseek(stm_s_data_fid, 8*fieldsize*(indexStart-1), 'bof');
    
    
    %Take care of the first blocks
    
    for k = 1: blocksNumber
        
        u_stm = fread(stm_u_data_fid,[fieldsize,blockLen],'double');
        s_stm = fread(stm_s_data_fid,[fieldsize,blockLen],'double');
        
        in_u_stm = interp2( 1:fieldsize , tsc.Time(indexStart+(k-1)*blockLen:indexStart+k*blockLen-1)' , [u_stm]' , 1:fieldsize , [tsc.Time(indexStart+(k-1)*blockLen):T:tsc.Time(indexStart+k*blockLen)]')';
        in_s_stm = interp2( 1:fieldsize , tsc.Time(indexStart+(k-1)*blockLen:indexStart+k*blockLen-1)' , [s_stm]' , 1:fieldsize , [tsc.Time(indexStart+(k-1)*blockLen):T:tsc.Time(indexStart+k*blockLen)]')';
        
        [~,len_interp] = size(in_u_stm);
        
        for i=1:len_interp
            
            %Updates the figure
            set(h1,'ydata',in_u_stm(:,i));
            frame = getframe(fig);
            
            %If its a movie, writes the frame to video obj
            writeVideo(writerObj,frame);
        end
    end
    %Take care of the last (incomplete block)
    
    u_stm = fread(stm_u_data_fid,[fieldsize,n_last],'double');
    s_stm = fread(stm_s_data_fid,[fieldsize,n_last],'double');
    
    in_u_stm = interp2( 1:fieldsize , tsc.Time(blocksNumber*blockLen+1:indexEnd)' , u_stm' , 1:fieldsize , [tsc.Time(indexStart+blocksNumber*blockLen+1):T:tsc.Time(indexEnd)]')';
    in_s_stm = interp2( 1:fieldsize , tsc.Time(blocksNumber*blockLen+1:indexEnd)' , s_stm' , 1:fieldsize , [tsc.Time(indexStart+blocksNumber*blockLen+1):T:tsc.Time(indexEnd)]')';
    
    [~,len_interp] = size(in_u_stm);
    
    for i=1:len_interp
        
        %Updates the figure
        set(h1,'ydata',in_u_stm(:,i));
        frame = getframe(fig);
        
        %If its a movie, writes the frame to video obj
        writeVideo(writerObj,frame);
    end
    close(writerObj);

    
    
    
end

end

