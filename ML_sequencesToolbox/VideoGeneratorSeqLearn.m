function VideoGeneratorSeqLearn(varargin)
%VideoGeneratorSeqLearn This function generates a video or a gif depicting
%the time evolution of the fields past and present of the SL module of the
%architecture.
%
%Work conducted by Emanuel Sousa
%
%Advisers: Estela Bicho and Wolfram Erlhagen
%
%Last update: 2014.07.07
%
%Main Inputs: 
%
% "fielddatastruc": a data structure containining four matrices Each matrix
%                   has the size of the fields versus the number of samples
%                   of the data. The matrices contain the data of "u_past",
%                   "s_past", "u_present" and "s_present".
%
% "tsc": a time serie collection whose time array describes the time array
%        of the data
%
% "fps": the desired value of frames per second for the video

%Initialize variables
tsc = [];
data_struct = [];
tstart = 0;
tend = 0;
lambda_pa = nan;
lambda_pr = nan;
fps = [];

maxylimpr = 5;

%standard file name
videofilename = 'newAnimation.avi';

%Get number of input arguments
argin = length(varargin);

%Interpret input arguments
for n = 1:1:argin
    arg = varargin{n};
    if isa(arg,'char')
        switch lower(arg)
            %Gets the tsc containing the time data
            case '-tsc'
                tsc = varargin{n+1};
            %Get the structure containing the fields    
            case '-fielddatastruc'
                data_struct = varargin{n+1};
            %Onset of the video
            case '-tstart'
                tstart = varargin{n+1};
            %Offset of the video
            case '-tend'
                tend = varargin{n+1};
            % define past learning threshold
            case '-lambda_pa'
                lambda_pa = varargin{n+1};
            % define present learning threshold
            case '-lambda_pr'
                lambda_pr = varargin{n+1};
            % Define labels for the plots
            case '-labels' 
                labels = varargin{n+1};
            %Frames per second
            case '-fps'
                fps = varargin{n+1};
            %Define if the purpose is to obtain a gif
            case '-gif'
                op = 'gif';
            %Define if the purpose is to obtain a video
            case '-vid'
                op = 'vid';
            %Define name of output file
            case '-outputfile'
                videofilename = varargin{n+1};
            case '-maxylimpr'
                maxylimpr = varargin{n+1};
                
        end
    end
end

%Verify if the mandatory stuff is given
assert(~isempty(data_struct), 'No data structure containing fields was passed');
assert(~isempty(fps),'Must define a value for fps');
  
% Calculates the time for each frame
T = 1/fps;

%If a time structure is passed
if ~isempty(tsc)
    %Get the index corresponding to the onset
    [~ ,indexStart] = min(abs(tsc.s_past.Time-tstart));
    
    %If a offset is passed get the index of the time frame closer to its 
    %value. Else the offset is the last value of the time array
    if tend~=0,
        [~ ,indexEnd] = min(abs(tsc.s_past.Time-tend));
    else
        indexEnd = length(tsc.s_past.Time);
    end
end

%Get structures containing the data between onset and offset
ts_u_past = data_struct.log_u_past_field(:,indexStart:indexEnd);
ts_s_past = data_struct.log_s_past_field(:,indexStart:indexEnd);
ts_u_pres = data_struct.log_u_present_field(:,indexStart:indexEnd);
ts_s_pres = data_struct.log_s_present_field(:,indexStart:indexEnd);

%Get number of frames
[fwidth,~] = size(ts_u_past);

%Generate figure
fig = figure('visible','off');

%et number of labels
labelLen = length(labels);

%calculate space between labels
spreadBetLabels = fwidth/labelLen;

%generate the xtick values
vecLabel = spreadBetLabels/2:spreadBetLabels:fwidth;

%plot the figures
subplot(2,1,1);
h1 = plot(ts_u_past(:,1)','r','LineWidth',3);
hold on
h4 = plot(ts_s_past(:,1)','b','LineWidth',2);
plot(zeros(1,fwidth),'k');
if ~isnan(lambda_pa)
    plot(lambda_pa * ones(1,fwidth),'k--');
end
ylim([-3 10]);
ylabel(gca,'u^{pa}(x^{pa},t)');
xlim([1 fwidth]);
xlabel(gca,'x^{pa}');
set(gca,'Xtick',vecLabel,'XTickLabel',labels);
title('Past Layer');

subplot(2,1,2);
h2 = plot(ts_u_pres(:,1)','r','LineWidth',3);
hold on
h3 = plot(ts_s_pres(:,1)','b','LineWidth',2);
plot(zeros(1,fwidth),'k');
if ~isnan(lambda_pr)
    plot(lambda_pr * ones(1,fwidth),'k--');
end
ylim([-5 maxylimpr]);
ylabel(gca,'u^{pr}(x^{pr},t)');
xlim([1 fwidth]);
set(gca,'Xtick',vecLabel,'XTickLabel',labels);
xlabel(gca,'x^{pr}');
title('Present Layer');

%sets the size of the print to be equal to the figure size
set(gcf,'PaperPositionMode','auto');

%interpolate data for getting a fixed time interval
disp('Doing the interpolations...');
in_u_past = interp2( 1:fwidth , tsc.Time(indexStart:indexEnd)' , [ts_u_past(:,indexStart:indexEnd)]' , 1:fwidth , [tsc.Time(indexStart):T:tsc.Time(indexEnd)]')';
in_s_past = interp2( 1:fwidth , tsc.Time(indexStart:indexEnd)' , [ts_s_past(:,indexStart:indexEnd)]' , 1:fwidth , [tsc.Time(indexStart):T:tsc.Time(indexEnd)]')';
in_u_pres = interp2( 1:fwidth , tsc.Time(indexStart:indexEnd)' , [ts_u_pres(:,indexStart:indexEnd)]' , 1:fwidth , [tsc.Time(indexStart):T:tsc.Time(indexEnd)]')';
in_s_pres = interp2( 1:fwidth , tsc.Time(indexStart:indexEnd)' , [ts_s_pres(:,indexStart:indexEnd)]' , 1:fwidth , [tsc.Time(indexStart):T:tsc.Time(indexEnd)]')';
disp('interpolations fineshed');
disp( '==========================_');
   
if strcmp(op,'gif')
    filename = 'newAnimation.gif';
elseif strcmp(op,'vid')
   
    % Create and open the video object
    writerObj = VideoWriter(videofilename);
    writerObj.FrameRate = fps;
    open(writerObj);

end
    
%Exact number of frames
[~,n_frames] = size(in_s_pres);
    
for i=1:n_frames,

    %Updates the figure
    set(h1,'ydata',in_u_past(:,i));
    set(h4,'ydata',in_s_past(:,i));
    set(h2,'ydata',in_u_pres(:,i));
    set(h3,'ydata',in_s_pres(:,i));
    frame = getframe(fig);

    %If it is a gif image, converts the frame to image and adds it to the
    %file
    if strcmp(op,'gif')
        im = frame2im(frame);
        [imind,cm] = rgb2ind(im,32,'nodither');
        if i == 1;
            imwrite(imind,cm,filename,'gif', 'Loopcount',inf,'DelayTime',0.08,'DisposalMethod','restoreBG','TransparentColor',1);
        else
            imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',0.08,'DisposalMethod','restoreBG','TransparentColor',1);
        end
        
    %If its a movie, writes the frame to video obj
    elseif strcmp(op,'vid')
        writeVideo(writerObj,frame);
    end
end

%If the option is for a movie, then the file must be closed
if strcmp(op,'mov')
    close(writerObj);
end
    

end

