function [meandurs,stanDiv] = CalculateMeanDurBetweenSubTasks( tsc,startTime,endTime )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
tsc2 = getsampleusingtime(tsc,startTime,endTime);

l = length(tsc2.Time);

are = reshape(tsc2.s_past.Data>0.5,10,l);


s = [];

for i=1:10,
    
    ge = find(are(i,:));
    if ~isempty(ge)
        aux = diff(ge);
        indexes = ge([1, find(aux>1)+1]);
        
        if isempty(s)
            s = tsc2.Time(indexes);
        else
            
            [ci,~] = size(s);
            
            s = [s,tsc2.Time(indexes(1:ci))];
        end
    end
end

[l,~] = size(s);

durs = diff(sort(s'))';


if l>1,

meandurs = mean(durs(2:end,:),1);

stanDiv = std(durs(2:end,:));
else
    
    meandurs = durs;
    stanDiv = zeros(1,l);
    
end

end

