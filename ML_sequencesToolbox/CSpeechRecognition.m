classdef CSpeechRecognition < handle
    %UNTITLED2 Summary of this class goes here
    %   Detailed explanation goes here
    
    properties(SetAccess = private, GetAccess = public)
        command_recognize
        yarpObject
        rfdata
        ruparam
        rCommand
        text
        interpreter_length
        interpreter
    end
    
    methods
        function obj = CSpeechRecognition (yarpObject , interpreter)
            obj.yarpObject = yarpObject;
            obj.command_recognize = 3806;
            obj.interpreter = interpreter;
            obj.interpreter_length = length(interpreter);
        end
        
        function recognize(obj)
            [obj.rfdata,obj.ruparam,obj.rCommand,obj.text] = ...
            obj.yarpObject.sendWithReply(obj.command_recognize,...
                                                    'give speech' ,[],[]);
        end
        
        function output = interpret(obj)
            output = 0;
            for i = 1:obj.interpreter_length
                if any(obj.interpreter{i,2} == obj.ruparam)
                    output = obj.interpreter{i,1};
                    break;
                end
            end
        end
    end
    
end

