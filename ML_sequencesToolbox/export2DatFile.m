function export2DatFile(header,matrix,filename)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here


len = length(header);
str = header{1};
for i = 2:len,
    str = [str,'\t',header{i}];
end

fid = fopen(filename,'wt');
fprintf(fid, [str,'\n']);
fclose(fid);

dlmwrite(filename,matrix,'-append','delimiter','\t');

end

