function center = CalculatePeakLocations( input )
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here

input_aux = input;
input_aux(input>0)=1;
input_aux(input<=0)=0;
crossings = diff(input_aux);

len = length(input);

%assuming a circular space
n_peaks = round((sum(crossings==1)+sum(crossings==-1))/2);

center = nan;

%One peak
if n_peaks == 1,
    
    in = find(crossings==1);
    
    if isempty(in)
        if input_aux(1)==1 && input_aux(end)==0,
            in = 1;
        end
    else
        in = in +1;
    end
    
    en = find(crossings==-1);
    if isempty(en)
       if input_aux(1)==0 && input_aux(end)==1,
           en = len;
       end
    end

    
    %the peak is not in the field limits
    if en>=in,
       center = in+(en-in)/2;
    else
        can = en + (in-en)/2+len/2;
        if can <len,
            center = can;
        else
            center = can-len;
        end
        
    end
    
    
    if center == 0,
        center = len;
    end
    
end



end

