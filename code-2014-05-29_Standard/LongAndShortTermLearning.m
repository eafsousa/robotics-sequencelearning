function varargout = LongAndShortTermLearning(varargin)
% LONGANDSHORTTERMLEARNING MATLAB code for LongAndShortTermLearning.fig
%      LONGANDSHORTTERMLEARNING, by itself, creates a new LONGANDSHORTTERMLEARNING or raises the existing
%      singleton*.
%
%      H = LONGANDSHORTTERMLEARNING returns the handle to a new LONGANDSHORTTERMLEARNING or the handle to
%      the existing singleton*.
%
%      LONGANDSHORTTERMLEARNING('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in LONGANDSHORTTERMLEARNING.M with the given input arguments.
%
%      LONGANDSHORTTERMLEARNING('Property','Value',...) creates a new LONGANDSHORTTERMLEARNING or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before LongAndShortTermLearning_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to LongAndShortTermLearning_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help LongAndShortTermLearning

% Last Modified by GUIDE v2.5 27-Feb-2014 16:33:42

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @LongAndShortTermLearning_OpeningFcn, ...
                   'gui_OutputFcn',  @LongAndShortTermLearning_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before LongAndShortTermLearning is made visible.
function LongAndShortTermLearning_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to LongAndShortTermLearning (see VARARGIN)

% Choose default command line output for LongAndShortTermLearning
handles.output = hObject;

parameters;

%% Robot Interfae
handles.aros = CAros('fake_sequences',p.sequences,'time_between_events',p.timeBetweenEvents,'period_variability',p.timervariability);
handles.aros.initiateRobotDevices('vision','fake','speechrecognition','fake');
handles.aros.startCommunications('vision','speechrecognition');


%% x vector
handles.x = p.x0:p.dx:p.size;

handles.poplength = p.poplength;

handles.labels =  p.labels;

%% flags
handles.flag_procIsRunning =0;
handles.flag_mode = 1; %by default is recalling. learning => 0
handles.flag_correct = 0;
handles.showPlots = 0;
handles.flag_acHighLev = 0;

%% stored values
handles.sigma_present = p.sigma_present;
handles.thr = 4;
handles.dt = 0.2;
handles.lambdaVec = p.lambdaVec;
handles.cnvKern = FRamp(EGauss(1,handles.x,p.size/2,3.0,0.1));
handles.lambdaNext = 1;
handles.size = p.size;
handles.upa = zeros(1,p.size);
handles.trainingEpochs = p.trainingEpochs;
handles.iterPerEpoch = p.iterPerEpoch;
handles.visionData = zeros(1,10);

%% counters
%inhibitory matrix training
handles.trainInibNum = 0;
% Long Term Learning
handles.demo_num = 0;
handles.demo_iter = 0;

%% High level to low level neural net
handles.A_high2low = CNeuralNet(p.size,p.size,zeros(p.size,p.size),1,1 ); 
handles.A_high2low.TAU = 8;
 
%% High level to low level neural net
handles.A_low2high = CNeuralNet(p.size,p.size,zeros(p.size,p.size),1,1 ); 
handles.A_low2high.TAU = 8;
 
 %% Two Layers Model 
handles.model = CLongTermShortMemory('x0',p.x0,...
                        'dx',p.dx,...
                        'xmax',p.size,...
                        'sigma_past1',p.sigma_past1,...
                        'sigma_past2',p.sigma_past2,...
                        'sigma_present',p.sigma_present,...
                        'kernel_amp_pa1',p.kernelAmpPa1,...
                        'kernel_amp_pa2',p.kernelAmpPa2,...
                        'kernel_amp_pr',p.kernelAmpPr,...
                        'kernel_inib_pa',0,...
                        'kernel_inib_pr',p.kernelInibPr,...
                        'g_vi_pa',p.G_vi_pa,...
                        'g_vi_pr',p.G_vi_pr,...
                        'g_pa2pr',p.G_pa2pr,...
                        'g_pr2pa',p.G_pr2pa,...
                        'g_recall',p.G_recall,...
                        'g_stm_pre',p.G_STM_pr,...
                        'g_vi_stm',p.G_vi_STM,...
                        'k_h_stm',p.k_h_STM,...
                        'u_stm_tau',p.u_STM_tau,...
                        'g_highlevinp',p.G_HighLevInp,...
                        'h_past_tau',p.h_TAU,...
                        'learning_tau',p.matrix_TAU,...
                        'h_past_ref_learning',p.h_past_learning,...
                        'h_past_ref_recall',p.h_past_recall,...
                        'h_past_low',p.h_past_low,...
                        'h_present_ref',p.h_present,...
                        'noise_amplitude',p.Namp,...
                        'lambda',p.lambdaVec(handles.lambdaNext),...
                        'beta',p.beta,...
                        'u_past_tau',p.u_TAU_past,...
                        'u_present_tau',p.u_TAU_present,...
                        'eta',p.eta,...
                        'populationLength',p.poplength,...
                        'usehcml',true,...
                        'u_hcml_tau',p.G_TAU *3,...
                        'u_tml_tau',p.G_TAU *3,...
                        'lambda_list',p.lambdaVec,...
                        'h_tml_high',p.h_tml_high,...
                        'h_tml_low',p.h_tml_low);

handles.model.setMode('recall');

%% u_HCML 
kern_HCML = 0.5*(EGauss(1.0,handles.x,p.size/2,p.sigma_present,0)-EGauss(0.5,handles.x,p.size/2,p.sigma_present*2,0));
handles.h_HCML = -2.0*ones(1,p.size);
handles.u_HCML = CDynamicNeuralField(p.dx, handles.h_HCML , kern_HCML ,p.G_TAU *3, 0, 2);
handles.s_HCML = zeros(1,p.size);
handles.s_HCML_err = zeros(1,p.size);
handles.s_HCML_inp = zeros(1,p.size);

%% u_TML
kern_TML = EGauss(1.0,handles.x,p.size/2,p.sigma_present/2,0.5);
handles.h_TML = -1.8*ones(1,p.size);
handles.u_TML = CDynamicNeuralField(p.dx, handles.h_TML , kern_TML ,p.G_TAU *3, 0, 0 );
handles.s_TML_VerbInp = zeros(1,p.size);
handles.s_TML = zeros(1,p.size);
handles.errorPopu_num = -1;

%% vector initialization
handles.presExcit = zeros(1,p.size);
handles.visualInput = zeros(1,p.size);
handles.s_past = zeros(1,p.size);
handles.inhibInp2HCML = zeros(1,p.size);

%% Plot of HCML
handles.h1 = plot(handles.axes_HCML,...
    handles.x , handles.model.u_HCML.u,...
    handles.x , handles.model.s_HCML,...
    handles.x , zeros(1,p.size),...
    handles.x , handles.thr*ones(1,p.size),...
    handles.x , handles.model.inhibInp2HCML);

set(handles.h1(1,1),'color','b','LineWidth',1.5,'LineStyle','-');
set(handles.h1(2,1),'color','r','LineWidth',1.5,'LineStyle','-');
set(handles.h1(4,1),'LineStyle','-');
set(handles.h1(5,1),'color','g','LineWidth',1.5,'LineStyle','--');
axis(handles.axes_HCML,[0 p.size -2 10]);
title(handles.axes_HCML,'HCML');

%% Plot of TML
handles.lh_TML = plot(handles.axes_TML,...
    handles.x , handles.model.u_TML.u,...
    handles.x , handles.model.s_TML);

set(handles.lh_TML(1,1),'color','r','LineWidth',1.5,'LineStyle','-');
set(handles.lh_TML(2,1),'color','b','LineWidth',1.5,'LineStyle','-');
axis(handles.axes_TML,[0 p.size -20 6]);
title(handles.axes_TML,'TML');

%% Plot of the hierarchical neural net
handles.h2 = mesh(handles.axes_MatHigh2Low,handles.A_high2low.A);
axis(handles.axes_MatHigh2Low,[1 , p.size , 1 , p.size , -0.1 , 0.3]);
title(handles.axes_MatHigh2Low,'High to low level connections');

%% Plot of the inhibitory neural net
handles.h3 = mesh(handles.axes_MatLow2High,-handles.A_low2high.A);
axis(handles.axes_MatLow2High,[1 , p.size , 1 , p.size , -0.1 , 0.3]);
title(handles.axes_MatLow2High,'Low to high level connections');

%% Plot of U past
handles.h4 = plot(handles.axes_upa, handles.x , handles.model.h_past ,...
    handles.x , handles.model.s_past ,...
    handles.x , handles.model.u_past.u ,...
    handles.x , handles.model.lambda*ones(1,p.size) );
set(handles.h4(1,1),'color','k','LineWidth',1.5,'LineStyle','--');
set(handles.h4(2,1),'color','b','LineWidth',1.5,'LineStyle','-');
set(handles.h4(3,1),'color','r','LineWidth',1.5,'LineStyle','-');
set(handles.h4(4,1),'color','g','LineWidth',1.5,'LineStyle','-');
legendcolorbarlayout(handles.axes_upa, 'remove');
axis(handles.axes_upa,[0 p.size -10 10]);
set(handles.axes_upa,'XTick',p.poplength/2:p.poplength:p.size-p.poplength/2,...
    'XTickLabel',p.labels,...
    'ZLimMode','manual',...
    'YTickLabelMode','manual',...
	'YTickMode','manual',...
    'ZTickLabelMode','manual',...
	'ZTickMode','manual');
title(handles.axes_upa,'CGG - Past');
grid(handles.axes_upa,'minor');
%% Plot of  U_PRESENT
handles.recall = zeros(1,p.size);
handles.h5 = plot(handles.axes_upr, handles.x , handles.model.h_present ,...
    handles.x , handles.model.recall ,...
    handles.x , handles.model.s_present ,...
    handles.x , handles.model.u_present.u ,...
    handles.x,p.beta*ones(1,p.size));
set(handles.h5(1,1),'Color','k','LineWidth',1.5,'LineStyle','--');
set(handles.h5(2,1),'Color','g','LineWidth',1.5,'LineStyle','-');
set(handles.h5(3,1),'color','b','LineWidth',1.5,'LineStyle','-');%,'EraseMode','none');
set(handles.h5(4,1),'color','r','LineWidth',1.5,'LineStyle','-');
legendcolorbarlayout(handles.axes_upr, 'remove');
axis(handles.axes_upr,[0 p.size -4 15]);
set(handles.axes_upr,'XTick',p.poplength/2:p.poplength:p.size-p.poplength/2,...
     'XTickLabel',p.labels,...
    'ZLimMode','manual',...
    'YTickLabelMode','manual',...
	'YTickMode','manual',...
    'ZTickLabelMode','manual',...
	'ZTickMode','manual');
title(handles.axes_upr,'CGG - Present');
grid(handles.axes_upr,'minor');

%% Plot of STM
handles.l_STM = plot(handles.axes_STM,...
    handles.x , handles.model.u_STM.u,...
    handles.x , handles.model.s_STM,...
    handles.x , handles.model.h_STM);

set(handles.l_STM(1,1),'color','r','LineWidth',1.5,'LineStyle','-');
set(handles.l_STM(2,1),'color','b','LineWidth',1.5,'LineStyle','-');
set(handles.l_STM(3,1),'color','g','LineWidth',1.5,'LineStyle','-');
axis(handles.axes_STM,[0 p.size -5 25]);
title(handles.axes_STM,'TML');


%% Plot of the Model Neural Net
handles.h6 = mesh(handles.axes_matr,handles.model.A);
axis(handles.axes_matr,[0 p.size 0 p.size -0.1 1]);
title(handles.axes_matr,'Past 2 present variable connections');

%% Load the fake memory traces for the short term memory

uEmanuel.a = load('memTraces_Real\Emanuel\[2013-05-29]SequenceMemory_Emanuel_1.mat');
uEmanuel.b = load('memTraces_Real\Emanuel\[2013-05-29]SequenceMemory_Emanuel_2.mat');
uEmanuel.c = load('memTraces_Real\Emanuel\SequenceMemory3.mat');

uRui.a = load('memTraces_Real\Rui\SequenceMemory1.mat');
uRui.b = load('memTraces_Real\Rui\SequenceMemory2.mat');
uRui.c = load('memTraces_Real\Rui\SequenceMemory3.mat');

uCarlos.a = load('memTraces_Real\Carlos\SequenceMemory1.mat');
uCarlos.b = load('memTraces_Real\Carlos\SequenceMemory2.mat');
uCarlos.c = load('memTraces_Real\Carlos\SequenceMemory3.mat');

u1 = uEmanuel.a.u_SM;
u2 = uEmanuel.b.u_SM;
u3 = uEmanuel.c.u_SM;

% u1 = uRui.a.u_SM;
% u2 = uRui.b.u_SM;
% u3 = uRui.c.u_SM;

% u1 = uCarlos.a.u_SM;
% u2 = uCarlos.b.u_SM;
% u3 = uCarlos.c.u_SM;

%Interpolates the fields for obtaining 360 neurons fields
handles.mem_traces = [interp1(1:180,u1(1:end-1),1:0.5:180,'linear'),-4.0;
              interp1(1:180,u2(1:end-1),1:0.5:180,'linear'),-4.0;
              interp1(1:180,u3(1:end-1),1:0.5:180,'linear'),-4.0];
 
%FOR TESTING PURPOSES
%handles.mem_traces(1,:) = EGauss(12,1:360,180,6,0);
%handles.mem_traces(1,:) = zeros(1,360);          
%END OF FOR TEST PURPOSES

handles.mem_traces(1,:)= handles.mem_traces(1,:)/12;
handles.mem_traces(2,:)= handles.mem_traces(2,:)/12;
handles.mem_traces(3,:)= handles.mem_traces(3,:)/12;

%Hides the base
%handles.mem_traces(:,1:60) = -0.36;

handles.currentMemTrace=zeros(1,p.size);

handles.errors = zeros(6,3000);
handles.dALog =  zeros(6,3000);
handles.dALogTotal = zeros(1,6000);
handles.dALogTotalIter = 1;
handles.dALogTotalEvents = [1];
handles.errorsIterator = ones(1,6);
handles.events = zeros(1,6);

handles.noiseLevel = p.Namp;
handles.recallGainLevel = p.G_recall;
handles.G_STMinput_Recall = p.G_STMinput_Recall;

handles.logPos = p.poplength/2:p.poplength:p.size-p.poplength/2;
handles.model.initializeLog(handles.logPos);
s = length(handles.logPos);
handles.log_visionInput = zeros(s,50000);
handles.log_shortMemory = zeros(s,50000);

handles.statistics.flag = 0;
handles.statistics.n_samples = 500;
handles.statistics.counter = handles.statistics.n_samples;
handles.statistics.decisionCounters = zeros(1,p.n_pops);

% Update handles structure
guidata(hObject, handles);

% UIWAIT makes LongAndShortTermLearning wait for user response (see UIRESUME)
% uiwait(handles.figure1);

% --- Outputs from this function are returned to the command line.
function varargout = LongAndShortTermLearning_OutputFcn(hObject, ~, handles) 
set(gcf,'units','normalized');
set(gcf,'Outerposition',[0 0.05 0.9 0.95]);
varargout{1} = handles.output;

% --------------------------------------------------------------------
function menu_trainLT_Callback(hObject, eventdata, handles)
handles.flag_mode = 0;
handles.model.set('noise_amplitude',0);
handles.trainingEpochs = 30;%51;%45;%36;
handles.model.setMode('rehearsal');
handles.model.forceResetFields('present');
handles.model.forceResetFields('past');
handles.events(end+1,:) = handles.errorsIterator;
handles.dALogTotalEvents(end+1,:) = handles.dALogTotalIter;
guidata(hObject, handles);

% --------------------------------------------------------------------
function menu_recall_Callback(hObject, eventdata, handles)
handles.model.setMode('recall');

% --------------------------------------------------------------------
function menu_startExec_Callback(hObject, eventdata, handles)
if ~handles.flag_procIsRunning
    %update state flag
    handles.flag_procIsRunning = 1;
    % Update handles structure
    guidata(hObject, handles);
    %starts the time counter
    handles.model.startTic();
    % starts the processing
    while(handles.flag_procIsRunning)
        %Checks if the window is open
        if ishghandle(hObject)
            handles = guidata(hObject);
            
            switch handles.aros.avaliateHumanSpeech()
                case handles.aros.speechIntent.TASKLEARNING_CORRECT_RECALL
        
                case handles.aros.speechIntent.TASKLEARNING_GOTO_LEARN
                    %
                    
                case handles.aros.speechIntent.TASKLEARNING_GOTO_RECALL
                    %Interrupts learning if necessary. Starts the recall
                    %proceeding
                    
                case handles.aros.speechIntent.TASKLEARNING_ROBOT_ERROR
                    %If the humans warns the robot that it made an error
                    menu_error_Callback(hObject,[], handles);
            end
            
            if handles.model.mode == 0,%learning
                handles = MainProcessRehearsal ( handles );
            elseif handles.model.mode == 1,
                handles = MainProcessRecall( handles );
            else
                handles = MainprocessDemonstration( handles );
            end
            guidata(hObject, handles);
            %If the teacher issues the string "correct"
            if handles.flag_correct
                %If the correct corresponds to the one population that
                %caused the error
                if(handles.model.errorPopu_num == handles.model.getActivPopID())
                    handles = TrainInibNet( handles );
                end
            end
            
            if handles.flag_acHighLev
                %If the correct corresponds to the one population that
                %caused the error
                handles.s_HCML_err = zeros(1,handles.size);
                handles.s_HCML_inp = EGauss(3,handles.x,handles.size/2,7.0,0.1);
                handles.s_HCML = handles.s_HCML_err + handles.s_HCML_inp;
                set(handles.h1(2,1),'ydata',handles.s_HCML);
                handles.flag_acHighLev = 0;
                guidata(hObject, handles);
            end
            
            %Adds the data to the time series
            handles.model.logData(0);
            handles.log_visionInput(:,handles.model.iter) = handles.visualInput(1,handles.logPos)';
            handles.log_shortMemory(:,handles.model.iter) = handles.currentMemTrace(1,handles.logPos)';
            
            %Updates data structure
            guidata(hObject, handles);
        else
            break
        end
        drawnow
        %pause(0.001);
    end
else
    disp('Process is already running!');
end

% --------------------------------------------------------------------
function menu_set_mem1_Callback(hObject, eventdata, handles)
handles.currentMemTrace=handles.mem_traces(1,:);
guidata(hObject, handles);
figure;plot(handles.mem_traces');
% --------------------------------------------------------------------
function menu_set_mem2_Callback(hObject, eventdata, handles)
handles.currentMemTrace=handles.mem_traces(2,:);
handles.trainingEpochs = 1;
handles.flag_mode = 0;
handles.model.setMode('rehearsal');
handles.model.forceResetFields('present');
handles.model.forceResetFields('past');
handles.events(end+1,:) = handles.errorsIterator;
guidata(hObject, handles);
figure;plot(handles.mem_traces');
% --------------------------------------------------------------------
function menu_set_mem3_Callback(hObject, eventdata, handles)
handles.currentMemTrace=handles.mem_traces(3,:);
guidata(hObject, handles);
figure;plot(handles.mem_traces');

% --------------------------------------------------------------------
function menu_set_none_Callback(hObject, eventdata, handles)
handles.currentMemTrace = zeros(1,handles.size);
guidata(hObject, handles);


% When the robot correctly learned the task (the error is corrected), this 
%function is called to create the links that allow the representations in
%u_pa to inhibit the high level representation
function handles = TrainInibNet( hnd )
handles = hnd;
if handles.trainInibNum < 50,
    handles.trainInibNum = handles.trainInibNum + 1;
    handles.model.trainInibNet(handles.dt);
    set(handles.h3,'zdata',-handles.model.A_low2high.A);
else
    handles.flag_correct = 0;
    handles.model.resetError();
end

function handles = MainProcessRehearsal(hnd)
handles = hnd;

if handles.demo_num < hnd.trainingEpochs,
    if handles.demo_iter < handles.iterPerEpoch,
        handles.demo_iter = handles.demo_iter + 1;
        
        %Sets the input used for training
        %handles.mem_input = 15 + 40*handles.currentMemTrace;
        handles.mem_input = handles.currentMemTrace; 
        field_input = zeros(1,handles.size);
        
        %trains de model
        handles.model.iterate(field_input,handles.mem_input,[],handles.dt);

        handles = logData(handles);
        
        if strcmp(get(handles.menu_showFieldLearning, 'Checked'),'on')
            updateGraphs(handles);
            set(handles.h6,'zdata',handles.model.A);
            set(handles.h2,'zdata',handles.model.A_high2low.A);
        end
        
        %If it reached the end of the epoch
    elseif handles.demo_iter == handles.iterPerEpoch
        handles.demo_iter = handles.demo_iter + 1;
        %clears the input
        %Resets the fields preparing for a new training epoch
        handles.model.resetPast();
        handles.model.u_present.reset();
        %Gives time for field relax to zeros
    elseif handles.demo_iter < handles.iterPerEpoch+30,
        handles.demo_iter = handles.demo_iter + 1;
        field_input = zeros(1,handles.size);
        handles.model.iterate(field_input,field_input,[],handles.dt);
        
    else
        handles.demo_iter = 0;
        handles.demo_num = handles.demo_num + 1;
        %updates the plots
        updateGraphs(handles);
        set(handles.h6,'zdata',handles.model.A);
        set(handles.h2,'zdata',handles.model.A_high2low.A);
    end
    
else %If it reached the end of demos go to Recall
    handles.flag_mode = 1;
    handles.model.set('noise_amplitude',handles.noiseLevel);
    handles.demo_num = 0;
    handles.model.setMode('recall');
    msgbox('Done learning') ;
    handles.aros.resetVisualInput();
end


function handles = MainprocessDemonstration( hnd )

handles = hnd;

%Gets info from the robot
vis = handles.aros.getVisualInput();


if any(vis~=handles.visionData)
    handles.visionData = vis;
    %builds the input vector
    handles.visualInput = FCreateGaussianPeaks(handles.size,handles.visionData([1:10]).*(handles.poplength/2:handles.poplength:handles.size-handles.poplength/2),1,1,2,0);
end

handles.model.iterate(handles.visualInput,zeros(1,handles.size),...
    handles.A_high2low.propagate(FStep(handles.u_HCML.u)),...
    handles.dt);

% handles.upa = handles.model.u_past.u;
handles.upr = single(handles.model.u_present.u);
handles.upa = single(handles.model.u_past.u);
% Refresh the image
if ~handles.statistics.flag
    updateGraphs(handles);
else
    % updateGraphs(handles);
    handles = statisticCounter(handles);
end



function handles = MainProcessRecall( hnd )

handles = hnd;

%Gets info from the robot
vis = handles.aros.getVisualInput();

if any(vis~=handles.visionData)
    handles.visionData = vis;
    %builds the input vector
    if any(handles.visionData~=0)
       a = 1; 
    end
    handles.visualInput = FCreateGaussianPeaks(handles.size,handles.visionData([1:10]).*(handles.poplength/2:handles.poplength:handles.size-handles.poplength/2),1,1,2,0);
end

%field_input = handles.G_STMinput_Recall*handles.currentMemTrace handles.visualInput;

handles.model.iterate(handles.visualInput,0.06*handles.currentMemTrace,...
    handles.A_high2low.propagate(FStep(handles.u_HCML.u)),...
    handles.dt);

% handles.upa = handles.model.u_past.u;
handles.upr = single(handles.model.u_present.u);
handles.upa = single(handles.model.u_past.u);
% Refresh the image
if ~handles.statistics.flag
    updateGraphs(handles);
else
    % updateGraphs(handles);
    handles = statisticCounter(handles);
end
    
%function for updating graphs
function updateGraphs(handles)

%set(handles.h4(1,1),'ydata',handles.model.h_past);
set(handles.h4(2,1),'ydata',handles.model.s_past);
set(handles.h4(3,1),'ydata',handles.model.u_past.u);

%set(handles.h5(1,1),'ydata',handles.model.h_present);
set(handles.h5(2,1),'ydata',handles.model.recall);
set(handles.h5(3,1),'ydata',handles.model.s_present);
set(handles.h5(4,1),'ydata',handles.model.u_present.u);

if handles.model.flag_editingParams
    set(handles.h4(4,1),'ydata',handles.model.lambda*ones(1,handles.size));
end

if handles.model.mode == 2,
    set( handles.l_STM(1,1),'ydata',handles.model.u_STM.u);
    set( handles.l_STM(2,1),'ydata',handles.model.s_STM);
    set( handles.l_STM(3,1),'ydata',handles.model.h_STM);
    set(handles.h6,'zdata',handles.model.A);
end

set(handles.h1(1,1),'ydata',handles.model.u_HCML.u);
set(handles.h1(2,1),'ydata',handles.model.s_HCML);
set(handles.h1(5,1),'ydata',handles.model.inhibInp2HCML);

set(handles.lh_TML(1,1),'ydata',handles.model.u_TML.u);
set(handles.lh_TML(2,1),'ydata',handles.model.s_TML);

set(handles.h2,'zdata',handles.model.A_high2low.A);

% --------------------------------------------------------------------
function menu_reset_allfields_Callback(hObject, eventdata, handles)
handles.model.forceResetFields('present');
handles.model.forceResetFields('past');

% --------------------------------------------------------------------
function menu_reset_uPresent_Callback(hObject, eventdata, handles)
handles.model.forceResetFields('present');

% --------------------------------------------------------------------
function menu_error_Callback(hObject, eventdata, handles)
handles.errorPopu_num = handles.model.getActivPopID();
handles.model.errorTrigger();
set(handles.h1(2,1),'ydata',handles.model.s_HCML);
set(handles.h4(4,1),'ydata',handles.model.lambda*ones(1,handles.size));
guidata(hObject, handles);

% --------------------------------------------------------------------
function menu_actHLrep_Callback(hObject, eventdata, handles)
handles.flag_acHighLev = 1;
guidata(hObject, handles);

% --------------------------------------------------------------------
function menu_plotMatrixSum_Callback(hObject, eventdata, handles)
figure;plot(sum(handles.model.A));

% --- Executes when user attempts to close figure1.
function figure1_CloseRequestFcn(hObject, eventdata, handles)
handles.aros.stopCommunications('all');
close all
handles.flag_procIsRunning = 0;
% Update handles structure
guidata(hObject, handles);
delete(hObject);

% --------------------------------------------------------------------
function menu_correct_Callback(hObject, eventdata, handles)
handles.flag_correct = 1;
guidata(hObject, handles);

% --------------------------------------------------------------------
function menu_loadModelMatrix_Callback(hObject, eventdata, handles)
aux = CNeuralNet(0,0,0,0,0);
aux.LoadMatrixSelectFile('./matrices');
handles.model.set('matr_init_val',aux.A);
set(handles.h6,'zdata',handles.model.A);
guidata(hObject, handles);

% --------------------------------------------------------------------
function menu_SaveModelMatrix_Callback(hObject, eventdata, handles)
aux = CNeuralNet(0,0,0,0,0);
aux.setW(handles.model.A);
aux.SaveMatrixSelectFile('./matrices');

% --------------------------------------------------------------------
function menu_loadHierarchicalMatrix_Callback(hObject, eventdata, handles)
handles.A_high2low.LoadMatrixSelectFile('./matrices');
set(handles.h2,'zdata',handles.A_high2low.A);
guidata(hObject, handles);

% --------------------------------------------------------------------
function menu_saveHierarchicalMatrix_Callback(hObject, eventdata, handles)
handles.A_high2low.SaveMatrixSelectFile('./matrices');

% --------------------------------------------------------------------
function menu_loadInibNatrix_Callback(hObject, eventdata, handles)
handles.A_low2high.LoadMatrixSelectFile('./matrices');
set(handles.h3,'zdata',-handles.A_low2high.A);
guidata(hObject, handles);

% --------------------------------------------------------------------
function menu_saveInibNatrix_Callback(hObject, eventdata, handles)
handles.A_low2high.SaveMatrixSelectFile('./matrices');

% --------------------------------------------------------------------
function ex=logData(handles)
presentAboveThreshold = FRamp(handles.model.u_present.u - handles.model.beta);
past = FRamp(handles.model.u_past.u);
if any(presentAboveThreshold) && any(past)
    [~,I] = max(presentAboveThreshold);
    event = 1+floor(I/60);
    handles.errors(event,handles.errorsIterator(event)) = handles.model.error(30 + 60*(event-1));
    a = sum(abs(handles.model.dA));
    handles.dALog(event,handles.errorsIterator(event)) = a(30 + 60*(event-1));
    handles.dALogTotal(handles.dALogTotalIter) = sum(a);
    handles.dALogTotalIter = handles.dALogTotalIter + 1;
    handles.errorsIterator(event) = handles.errorsIterator(event)+1;
end
ex = handles;

% --------------------------------------------------------------------
function menu_saveLearningLog_Callback(hObject, eventdata, handles)
si = max(handles.errorsIterator);

ts1 = timeseries(handles.errors(1,1:si), 1:si,'name', 'E_BA');
s = handles.events(2:end,1);
if ~isempty(s)
    ts1 = addevent(ts1,cellstr(num2str(s)),num2cell(s));
end

ts2 = timeseries(handles.errors(2,1:si), 1:si,'name', 'E_C1');
s = handles.events(2:end,2);
if ~isempty(s)
    ts2 = addevent(ts2,cellstr(num2str(s)),num2cell(s));
end
   
ts3 = timeseries(handles.errors(3,1:si), 1:si,'name', 'E_C2');
s = handles.events(2:end,3);
if ~isempty(s)
    ts3 = addevent(ts3,cellstr(num2str(s)),num2cell(s));
end

ts4 = timeseries(handles.errors(4,1:si), 1:si,'name', 'E_C3');
s = handles.events(2:end,4);
if ~isempty(s)
    ts4 = addevent(ts4,cellstr(num2str(s)),num2cell(s));
end

ts5 = timeseries(handles.errors(5,1:si), 1:si,'name', 'E_C4');
s = handles.events(2:end,5);
if ~isempty(s)
    ts5 = addevent(ts5,cellstr(num2str(s)),num2cell(s));
end

ts6 = timeseries(handles.errors(6,1:si), 1:si,'name', 'E_TP');
s = handles.events(2:end,6);
if ~isempty(s)
    ts6 = addevent(ts6,cellstr(num2str(s)),num2cell(s));
end

ts7 = timeseries(handles.dALog(1,1:si), 1:si,'name', 'V_BA');
s = handles.events(2:end,1);
if ~isempty(s)
    ts7 = addevent(ts7,cellstr(num2str(s)),num2cell(s));
end

ts8 = timeseries(handles.dALog(2,1:si), 1:si,'name', 'V_C1');
s = handles.events(2:end,2);
if ~isempty(s)
    ts8 = addevent(ts8,cellstr(num2str(s)),num2cell(s));
end

ts9 = timeseries(handles.dALog(3,1:si), 1:si,'name', 'V_C2');
s = handles.events(2:end,3);
if ~isempty(s)
    ts9 = addevent(ts9,cellstr(num2str(s)),num2cell(s));
end
    
ts10 = timeseries(handles.dALog(4,1:si), 1:si,'name', 'V_C3');
s = handles.events(2:end,4);
if ~isempty(s)
    ts10 = addevent(ts10,cellstr(num2str(s)),num2cell(s));
end

ts11 = timeseries(handles.dALog(5,1:si), 1:si,'name', 'V_C4');
s = handles.events(2:end,5);
if ~isempty(s)
    ts11 = addevent(ts11,cellstr(num2str(s)),num2cell(s));
end

ts12 = timeseries(handles.dALog(6,1:si), 1:si,'name', 'V_TP');
s = handles.events(2:end,6);
if ~isempty(s)
    ts12 = addevent(ts12,cellstr(num2str(s)),num2cell(s));
end

tsc_learning = tscollection({ts1 ts2 ts3 ts4 ts5 ts6 ts7 ts8 ts9 ts10 ts11 ts12},'name','tsc_learning');
save('tsc_learning.mat','tsc_learning');

sd = handles.dALogTotalIter;
s = handles.dALogTotalEvents(2:end);

ts1 = timeseries(handles.dALogTotal(1,1:sd), 1:sd,'name', 'Total');

if ~isempty(s)
    ts1 = addevent(ts1,cellstr(num2str(s)),num2cell(s));
end
tsc_dAT = tscollection({ts1},'name','tsc_dAT');
save('tsc_dAT.mat','tsc_dAT');

%Gets the logged data from the model
tsc = handles.model.getLogData(handles.labels);
%adds logs from vision and shortTermMemory
ts1 = handles.model.createTSerie(handles.log_visionInput,'visionInput',handles.labels);
tsc = addts(tsc,ts1);
ts2 = handles.model.createTSerie(handles.log_shortMemory,'shortMemory',handles.labels);
tsc = addts(tsc,ts2);
save([FGetFormattedDate,'tsc_fields_logs.mat'],'tsc');


%tsc2 = handles.model.clusterFieldLogs();
%save('complete_fields.mat','tsc2');
struc = handles.model.clusterFieldLogsStruct();


save([FGetFormattedDate,'_complete_fields.mat'],'struc');

% --------------------------------------------------------------------
function menu_set_noise_level_Callback(hObject, eventdata, handles)
hnd = slidevar('Noise level',{@noisechange,hObject},[0,15],handles.noiseLevel);

% --------------------------------------------------------------------
function noisechange(varargin)

ht = findobj(gcbf, 'style','text');
v = get(varargin{1},'value');
set(ht, 'string', num2str(v))
handles = guidata(varargin{3});
handles.model.set('noise_amplitude',v);
handles.noiseLevel = v;
guidata(varargin{3}, handles);

% --------------------------------------------------------------------
function h=slidevar(varname,callback,span,initVal)

if nargin==4
    h = figure('pos',[300 300 300 80],...
        'menubar','none',...
        'numbertitle','off',...
        'name',['Variable: ' varname]);

    hs = uicontrol(h,...
        'style','slider',...
        'pos',[10 40 280 20],...
        'min',span(1),...
        'max',span(2),...
        'value',initVal,...
        'callback',callback,...
        'tag',varname);
    
    uicontrol(h,...
        'style','text',...
        'pos',[10 10 280 20],...
        'tag',['text_' varname],...
        'string',num2str(initVal))

    if nargout==0
        clear h
    end
end

% --------------------------------------------------------------------
function menu_showFieldLearning_Callback(hObject, eventdata, handles)
if strcmp(get(hObject, 'Checked'),'on')
    set(hObject, 'Checked', 'off');
else 
    set(hObject, 'Checked', 'on');
end

% --------------------------------------------------------------------
function menu_errorStatistics_Callback(hObject, eventdata, handles)
handles.statistics.flag = 1;
handles.model.freezePast(true);
handles.statistics.decisionCounters(1,:)=0;
handles.tID = tic;
guidata(hObject, handles);

% --------------------------------------------------------------------
function  handles = statisticCounter(hnd)
handles = hnd;
if handles.statistics.counter>0,
    [C,I] = max(handles.model.u_present.u);
    if C>1 %if a decision is made
        pop = round((I+30)/60);
        handles.statistics.decisionCounters(1,pop) = handles.statistics.decisionCounters(1,pop)+1;
        handles.model.forceResetFields('present');
        disp(['Trials missing:',num2str(handles.statistics.counter),...
            '|decision=',num2str(pop),...
            '|Neuron:',num2str(I),...
            '|Last Decision:',num2str(toc(handles.tID)),'s']);
        handles.tID = tic;
        handles.statistics.counter = handles.statistics.counter - 1;
    end
else
    handles.statistics.flag = 0;
    handles.model.freezePast(false);
    handles.statistics.counter = handles.statistics.n_samples;
    disp(handles.statistics.decisionCounters/handles.statistics.n_samples);
end

% Empty function automatic generated----------------------------------
function Untitled_5_Callback(hObject, eventdata, handles)
function Untitled_4_Callback(hObject, eventdata, handles)
function menu_actions_Callback(hObject, eventdata, handles)
function Untitled_2_Callback(hObject, eventdata, handles)
function menu_trainST_Callback(hObject, eventdata, handles)
function Untitled_1_Callback(hObject, eventdata, handles)
function modes_menu_Callback(hObject, eventdata, handles)
function Untitled_6_Callback(hObject, eventdata, handles)
function Untitled_7_Callback(hObject, eventdata, handles)
function Untitled_8_Callback(hObject, eventdata, handles)
function Untitled_9_Callback(hObject, eventdata, handles)


% --------------------------------------------------------------------
function menu_control_vision_Callback(hObject, eventdata, handles)
handles.aros.stopCommunications('vision');
if strcmp(get(hObject,'Checked'),'off')
    handles.aros.initiateRobotDevices('vision','real');
    handles.aros.startCommunications('vision');
    set(hObject,'Checked','on');
else
    handles.aros.initiateRobotDevices('vision','fake');
    handles.aros.startCommunications('vision');
    set(hObject,'Checked','off');
end
    

% --------------------------------------------------------------------
function menu_control_speechProd_Callback(hObject, eventdata, handles)
handles.aros.stopCommunications('speechsynthesis');
if strcmp(get(hObject,'Checked'),'off')
    handles.aros.initiateRobotDevices('speechsynthesis','real');
    handles.aros.startCommunications('speechsynthesis');
    set(hObject,'Checked','on');
else
    handles.aros.initiateRobotDevices('speechsynthesis','fake');
    handles.aros.startCommunications('speechsynthesis');
    set(hObject,'Checked','off');
end


% --------------------------------------------------------------------
function menu_control_speechRecon_Callback(hObject, eventdata, handles)
handles.aros.stopCommunications('speechrecognition');
if strcmp(get(hObject,'Checked'),'off')
    handles.aros.initiateRobotDevices('speechrecognition','real');
    handles.aros.startCommunications('speechrecognition');
    set(hObject,'Checked','on');
else
    handles.aros.initiateRobotDevices('speechrecognition','fake');
    handles.aros.startCommunications('speechrecognition');
    set(hObject,'Checked','off');
end


% --------------------------------------------------------------------
function menu_control_arm_Callback(hObject, eventdata, handles)
handles.aros.stopCommunications('actionmanager');
if strcmp(get(hObject,'Checked'),'off')
    handles.aros.initiateRobotDevices('actionmanager','real');
    handles.aros.startCommunications('actionmanager');
    set(hObject,'Checked','on');
else
    handles.aros.initiateRobotDevices('actionmanager','fake');
    handles.aros.startCommunications('actionmanager');
    set(hObject,'Checked','off');
end


% --------------------------------------------------------------------
function menu_setLambdaThresholds_Callback(hObject, eventdata, handles)
handles.model.setLearningThresholdsGUI();

% --------------------------------------------------------------------
function m_startRecFields_Callback(hObject, eventdata, handles)
handles.model.startFieldsLog();

% --------------------------------------------------------------------
function m_stopRecFields_Callback(hObject, eventdata, handles)
handles.model.stopFieldsLog();


% --------------------------------------------------------------------
function menu_demonstrate_Callback(hObject, eventdata, handles)
handles.model.setMode('demonstration');


% --------------------------------------------------------------------
function m_endedDemo_Callback(hObject, eventdata, handles)
handles.model.setMode('rehearsal');
handles.trainingEpochs = 30;%51;%45;%36;
handles.model.forceResetFields('present');
handles.model.forceResetFields('past');
handles.events(end+1,:) = handles.errorsIterator;
handles.dALogTotalEvents(end+1,:) = handles.dALogTotalIter;


% --------------------------------------------------------------------
function m_setRecallGain_Callback(hObject, eventdata, handles)
hnd = slidevar('Recall Gain',{@recallGainChange,hObject},[0,3],handles.recallGainLevel);

function recallGainChange(varargin)

ht = findobj(gcbf, 'style','text');
v = get(varargin{1},'value');
set(ht, 'string', num2str(v))
handles = guidata(varargin{3});
handles.model.set('g_recall',v);
handles.recallGainLevel = v;
guidata(varargin{3}, handles);
