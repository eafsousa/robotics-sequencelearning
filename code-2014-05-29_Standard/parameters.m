%% Sizes===================================================================
p.x0 = 1;
p.dx = 1;
p.poplength = 30;
p.n_pops = 10;
p.size = p.poplength*p.n_pops;
p.G_TAU = 0.2;%10;
p.Namp = 5;
p.kernelGloInib = 2;%25;
%% U_PAST==================================================================
p.kernelAmpPa1 = 5.85;
p.kernelAmpPa2 = 3.87;

p.sigma_past1 = 2;
p.sigma_past2 = 3;
p.h_past = -2.0; %-18
p.G_vi_pa = 1.0;

p.u_TAU_past = p.G_TAU*20;%*15;%30;
p.h_past_learning = -1.0;%.5;
p.h_past_recall = -0.05;
p.h_TAU = 2.8;
p.h_past_low = -6.5;
%% U_PRESENT===============================================================
p.kernelAmpPr = 9.5;%28;
p.kernelInibPr = 9;
p.u_TAU_present = p.G_TAU*10;
p.sigma_present = 8.0/2; %1.6
p.G_vi_pr = 15;%200;
p.G_STM_pr = 1;%200;
p.h_present = -3.0;
p.G_OML2AEL = 10;
%% CONNECTIONS=============================================================
p.G_pa2pr = 6;%20;
p.G_pr2pa = 100;%2;
p.G_recall = 2.5;
p.G_HighLevInp = 0.8;
p.G_STMinput_Recall = 1.3;
%% LEARNING================================================================
p.d_TAU_pos = 5;
p.d_TAU_neg = -.8;
p.matrix_TAU = p.G_TAU *9;%5.0;
p.eta = 0.1;


%% TML=============================================
p.h_tml_low = -8;
p.h_tml_high= -1;



%% THRESHOLDS=============================================================
p.beta =5;% 10;        %Present Learning Threshold

p.lambda = 5.3;     %past Learning Threshold
% 7.7 one to one
% 6.5 two to one 
% 5.8 three to one
% 5.3 four to one
p.lambdaVec = [7.4 6.5 5.5 4.5 4.0];

p.timeBetweenEvents = 3;
p.tasksToExecute = 1;
p.trainingEpochs = 10;%30;
p.iterPerEpoch = 180;

p.timervariability = 2;

p.G_vi_STM = 7;

p.k_h_STM=1/20;

p.u_STM_tau = p.G_TAU*10;

%Flora
 p.labels = {'BP';'LW';'RW';'LN';'LW';'C1';'C2';'C3';'C4';'TP'};

 p.sequences = [ 1 6 7 8 9 10;...
                 1 9 8 7 6 10;...
                 1 6 9 8 7 10];


