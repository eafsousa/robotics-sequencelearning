function [ output_args ] = frameGenerator( tsc, data_struct, tstart, tend ,lambda_pa,lambda_pr,labels,op)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here


[~ ,indexStart] = min(abs(tsc.s_past.Time-tstart));
[~ ,indexEnd] = min(abs(tsc.s_past.Time-tend));

ts_u_past = data_struct.log_u_past_field(:,indexStart:indexEnd);
ts_s_past = data_struct.log_s_past_field(:,indexStart:indexEnd);
ts_u_pres = data_struct.log_u_present_field(:,indexStart:indexEnd);
ts_s_pres = data_struct.log_s_present_field(:,indexStart:indexEnd);

[fwidth,len] = size(ts_u_past);

fig = figure('visible','off');

labelLen = length(labels);

spreadBetLabels = fwidth/labelLen;

vecLabel = spreadBetLabels/2:spreadBetLabels:fwidth;

subplot(2,1,1);
h1 = plot(ts_u_past(:,1)','r','LineWidth',3);
hold on
h4 = plot(ts_s_past(:,1)','b','LineWidth',2);
plot(zeros(1,fwidth),'k');
plot(lambda_pa * ones(1,fwidth),'k--');
ylim([-3 10]);
ylabel(gca,'u^{pa}(x^{pa},t)');
xlim([1 fwidth]);
xlabel(gca,'x^{pa}');
set(gca,'Xtick',vecLabel,'XTickLabel',labels);
title('Past Layer');

subplot(2,1,2);
h2 = plot(ts_u_pres(:,1)','r','LineWidth',3);
hold on
h3 = plot(ts_s_pres(:,1)','b','LineWidth',2);
plot(zeros(1,fwidth),'k');
plot(lambda_pr * ones(1,fwidth),'k--');
ylim([-5 5]);
ylabel(gca,'u^{pr}(x^{pr},t)');
xlim([1 fwidth]);
set(gca,'Xtick',vecLabel,'XTickLabel',labels);
xlabel(gca,'x^{pr}');
title('Present Layer');

    %sets the size of the print to be equal to the figure size
    set(gcf,'PaperPositionMode','auto');

    
if strcmp(op,'gif')
    filename = 'newAnimation.gif';
elseif strcmp(op,'mov')
    writerObj = VideoWriter('newAnimation.avi');
    open(writerObj);
end
    
    
for i=1:len,

    %Updates the figure
    set(h1,'ydata',ts_u_past(:,i));
    set(h4,'ydata',3*ts_s_past(:,i));
    set(h2,'ydata',ts_u_pres(:,i));
    set(h3,'ydata',ts_s_pres(:,i));
    frame = getframe(fig);

    %If it is a gif image, converts the frame to image and adds it to the
    %file
    if strcmp(op,'gif')
        im = frame2im(frame);
        [imind,cm] = rgb2ind(im,32,'nodither');
        if i == 1;
            imwrite(imind,cm,filename,'gif', 'Loopcount',inf,'DelayTime',0.08,'DisposalMethod','restoreBG','TransparentColor',1);
        else
            imwrite(imind,cm,filename,'gif','WriteMode','append','DelayTime',0.08,'DisposalMethod','restoreBG','TransparentColor',1);
        end
        
    %If its a movie, writes the frame to video obj
    elseif strcmp(op,'mov')
        writeVideo(writerObj,frame);
    end
end

%If the option is for a movie, then the file must be closed
if strcmp(op,'mov')
    close(writerObj);
end
    

end

