%Data Analysis

load('tsc_fields_logs.mat');

tsc.u_present.Data = FTrunkData(tsc.u_present.Data,-10,30);
tsc.s_present.Data = FTrunkData(tsc.s_present.Data,0,8);
tsc.recall.Data =  FTrunkData(tsc.recall.Data,-30,30);

TSVisualizer(tsc);