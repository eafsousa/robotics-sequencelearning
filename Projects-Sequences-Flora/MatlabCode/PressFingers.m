function varargout = PressFingers(varargin)
% PRESSFINGERS M-file for PressFingers.fig
%      PRESSFINGERS, by itself, creates a new PRESSFINGERS or raises the existing
%      singleton*.
%
%      H = PRESSFINGERS returns the handle to a new PRESSFINGERS or the handle to
%      the existing singleton*.
%
%      PRESSFINGERS('CALLBACK',hObject,eventData,handles,...) calls the local
%      function named CALLBACK in PRESSFINGERS.M with the given input arguments.
%
%      PRESSFINGERS('Property','Value',...) creates a new PRESSFINGERS or raises the
%      existing singleton*.  Starting from the left, property value pairs are
%      applied to the GUI before PressFingers_OpeningFcn gets called.  An
%      unrecognized property name or invalid value makes property application
%      stop.  All inputs are passed to PressFingers_OpeningFcn via varargin.
%
%      *See GUI Options on GUIDE's Tools menu.  Choose "GUI allows only one
%      instance to run (singleton)".
%
% See also: GUIDE, GUIDATA, GUIHANDLES

% Edit the above text to modify the response to help PressFingers

% Last Modified by GUIDE v2.5 01-Sep-2011 12:05:49

% Begin initialization code - DO NOT EDIT
gui_Singleton = 1;
gui_State = struct('gui_Name',       mfilename, ...
                   'gui_Singleton',  gui_Singleton, ...
                   'gui_OpeningFcn', @PressFingers_OpeningFcn, ...
                   'gui_OutputFcn',  @PressFingers_OutputFcn, ...
                   'gui_LayoutFcn',  [] , ...
                   'gui_Callback',   []);
if nargin && ischar(varargin{1})
    gui_State.gui_Callback = str2func(varargin{1});
end

if nargout
    [varargout{1:nargout}] = gui_mainfcn(gui_State, varargin{:});
else
    gui_mainfcn(gui_State, varargin{:});
end
% End initialization code - DO NOT EDIT


% --- Executes just before PressFingers is made visible.
function PressFingers_OpeningFcn(hObject, eventdata, handles, varargin)
% This function has no output args, see OutputFcn.
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
% varargin   command line arguments to PressFingers (see VARARGIN)

% Choose default command line output for PressFingers
handles.output = hObject;

handles.ActionManager = CYarpCommunication('/Control/HLC');

handles.pressingTime = 200;
% Update handles structure
guidata(hObject, handles);

% UIWAIT makes PressFingers wait for user response (see UIRESUME)
% uiwait(handles.figure1);


% --- Outputs from this function are returned to the command line.
function varargout = PressFingers_OutputFcn(hObject, eventdata, handles) 
% varargout  cell array for returning output args (see VARARGOUT);
% hObject    handle to figure
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Get default command line output from handles structure
varargout{1} = handles.output;


% --- Executes on button press in connect.
function connect_Callback(hObject, eventdata, handles)
% hObject    handle to connect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    handles.ActionManager.connectToDevice('/dumbo/am_matlab_server');
    disp('connect');

% --- Executes on button press in disconnect.
function disconnect_Callback(hObject, eventdata, handles)
% hObject    handle to disconnect (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    handles.ActionManager.closeCom();
    disp('Disconnect');

% --- Executes on button press in pressFinger1.
function pressFinger1_Callback(hObject, eventdata, handles)
% hObject    handle to pressFinger1 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    sCommand = 6808;
    sText = ' ';
    sfData = [1 , handles.pressingTime];
    suParam = [];
    sErrorcode = 0;

    handles.ActionManager.sendWithReply(sCommand,sText,sfData,suParam,sErrorcode);

% --- Executes on button press in pressFinger2.
function pressFinger2_Callback(hObject, eventdata, handles)
% hObject    handle to pressFinger2 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    sCommand = 6808;
    sText = ' ';
    sfData = [2 , handles.pressingTime];
    suParam = [];
    sErrorcode = 0;

    handles.ActionManager.sendWithReply(sCommand,sText,sfData,suParam,sErrorcode);

% --- Executes on button press in pressFinger3.
function pressFinger3_Callback(hObject, eventdata, handles)
% hObject    handle to pressFinger3 (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
    sCommand = 6808;
    sText = ' ';
    sfData = [3 , handles.pressingTime];
    suParam = [];
    sErrorcode = 0;
 
    [a,b,c,d,e] = handles.ActionManager.sendWithReply(sCommand,sText,sfData,suParam,sErrorcode);
    


function editTimeBox_Callback(hObject, eventdata, handles)
% hObject    handle to editTimeBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)

% Hints: get(hObject,'String') returns contents of editTimeBox as text
%        str2double(get(hObject,'String')) returns contents of editTimeBox as a double


% --- Executes during object creation, after setting all properties.
function editTimeBox_CreateFcn(hObject, eventdata, handles)
% hObject    handle to editTimeBox (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    empty - handles not created until after all CreateFcns called

% Hint: edit controls usually have a white background on Windows.
%       See ISPC and COMPUTER.
if ispc && isequal(get(hObject,'BackgroundColor'), get(0,'defaultUicontrolBackgroundColor'))
    set(hObject,'BackgroundColor','white');
end


% --- Executes on button press in setTime.
function setTime_Callback(hObject, eventdata, handles)
% hObject    handle to setTime (see GCBO)
% eventdata  reserved - to be defined in a future version of MATLAB
% handles    structure with handles and user data (see GUIDATA)
data = get(handles.editTimeBox);

handles.pressingTime = str2double(data.String);

guidata(hObject, handles);
