<map version="0.9.0">
<!-- To view this file, download free mind mapping software FreeMind from http://freemind.sourceforge.net -->
<node COLOR="#990000" CREATED="1352995401622" ID="ID_1655295893" MODIFIED="1353343565986" TEXT="Sequences">
<edge COLOR="#990000" WIDTH="1"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1352995668807" ID="ID_434552525" MODIFIED="1353514695186" POSITION="right" TEXT="Classical paradigms">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1352995680066" ID="ID_372615285" MODIFIED="1353346807871" TEXT="Chaining">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1353077137907" ID="ID_1309076410" MODIFIED="1353343565993" TEXT="First Approach">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1353077207849" ID="ID_1135579517" MODIFIED="1354292691458" TEXT="Ebbinghaus ">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1353077153554" ID="ID_123079808" MODIFIED="1353502066823" TEXT="Challenged by">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1353077248097" ID="ID_1179262900" MODIFIED="1353343565995" TEXT="Lashley1951">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1353077251385" ID="ID_1879466138" MODIFIED="1353343565995" TEXT="Henson1998">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1353080576257" ID="ID_467268423" MODIFIED="1353343565995" TEXT="Drawbacks">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1353080585353" ID="ID_1316239113" MODIFIED="1353343565996" TEXT="How to encode repeated items">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1353088290866" ID="ID_860035977" MODIFIED="1353343565996" TEXT="How to recover from an error">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1353088299738" ID="ID_1814344615" MODIFIED="1353343565996" TEXT="What triggers the sequence">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1353077343241" ID="ID_1385563253" MODIFIED="1353502065127" TEXT="Compond Chaining">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1353080462145" ID="ID_1682443164" MODIFIED="1353343565998" TEXT="TODAM models">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1353087785706" ID="ID_1652341480" MODIFIED="1353343566001">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Functionalist model
    </p>
  </body>
</html></richcontent>
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1353087880714" ID="ID_1354723509" MODIFIED="1353343566002" TEXT="unconcerned with implementation">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node COLOR="#111111" CREATED="1353087829226" ID="ID_1650025809" MODIFIED="1353343566002" TEXT="Not biologicallly plausible">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1353087933242" ID="ID_1895962120" MODIFIED="1353426673211" TEXT="TODAM1">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1353087961498" ID="ID_1563139581" MODIFIED="1353343566003" TEXT="Storage and Retrieval">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1353080467769" ID="ID_400674058" MODIFIED="1353343566003" TEXT="Murdock1983">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1353080486873" ID="ID_1670393112" MODIFIED="1353426431490" TEXT="Lewandowsky1989">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1353087941394" ID="ID_1855538281" MODIFIED="1353343566004" TEXT="TODAM2">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1353087944962" ID="ID_181968740" MODIFIED="1353343566004" TEXT="Chunking Model">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1353088002346" ID="ID_57406861" MODIFIED="1353343566004" TEXT="Murdock1993">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1353426643501" ID="ID_1201367122" MODIFIED="1353426671899" TEXT="distributed representation"/>
<node CREATED="1353512329394" ID="ID_319333171" MODIFIED="1353512346529" TEXT="Implicitely supresses recalled ittems"/>
</node>
<node CREATED="1357575149259" ID="ID_1247952114" MODIFIED="1357575153366" TEXT="SOB ">
<node CREATED="1357575157187" ID="ID_472610583" MODIFIED="1357575178478" TEXT="Farrell et Lewandowsky 2002">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1352995706239" ID="ID_435675859" MODIFIED="1357311580661" TEXT="Ordinal">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1352995695703" ID="ID_1575370423" MODIFIED="1353343566004" TEXT="Competitive Queing">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1353343366921" ID="ID_1051096563" MODIFIED="1353343566005" TEXT="Flora2011">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1353512214746" ID="ID_1261255129" MODIFIED="1353512224096" TEXT="Houghton1990">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1354203190959" ID="ID_633481869" MODIFIED="1354203221388" TEXT="Hartley et Houghton 1996">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1354642243996" ID="ID_175109520" MODIFIED="1380194506781" TEXT="Primacy gradients">
<node CREATED="1380194511391" ID="ID_2240099" MODIFIED="1380194520067" TEXT="Page1998">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1357311583328" ID="ID_173828152" MODIFIED="1357311597523" TEXT="Perturbation Model">
<node CREATED="1357311604008" ID="ID_946317051" MODIFIED="1357311666078" TEXT="Estes 1972">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1352995709887" ID="ID_161057223" MODIFIED="1353343566005" TEXT="Positional">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1353078633849" ID="ID_617116381" MODIFIED="1353343566005" TEXT="START-END model">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1353077435617" ID="ID_1514353542" MODIFIED="1353343566005" TEXT="Henson1998">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1353424505776" ID="ID_1662808599" MODIFIED="1353424555396">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Performance in sequence
    </p>
    <p>
      memorization is enhanced
    </p>
    <p>
      for the first and last elements
    </p>
    <p>
      of a seq.
    </p>
  </body>
</html></richcontent>
<node CREATED="1353424577005" ID="ID_1418980395" MODIFIED="1353424628483" TEXT="Henson1998">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1353424587622" ID="ID_1062198403" MODIFIED="1353424628483" TEXT="Henson1999a">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1353424603702" ID="ID_755210261" MODIFIED="1353424628483" TEXT="Henson1999b">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1353424613821" ID="ID_641564959" MODIFIED="1353424628483" TEXT="Hitch1996">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1354551318232" ID="ID_281208503" MODIFIED="1354551326447" TEXT="Box models">
<node CREATED="1353347900897" ID="ID_1716298040" MODIFIED="1353347911790" TEXT="Conrad1965">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1354641155755" ID="ID_111804940" MODIFIED="1354641166620" TEXT="OSCAR model">
<node CREATED="1354641167743" ID="ID_418183843" MODIFIED="1354641176016" TEXT="Brown et al 2000">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1357311620672" ID="ID_527569940" MODIFIED="1357311646763" TEXT="Dual-trace framework">
<node CREATED="1357311650353" ID="ID_1201090104" MODIFIED="1357311664598" TEXT="Estes 1997">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1352996187031" ID="ID_1019665566" MODIFIED="1353514731521" POSITION="left" TEXT="Recent approaches">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1352995633047" ID="ID_484392865" MODIFIED="1353343566008" TEXT="Connectionist ">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1353000849992" ID="ID_1185157214" MODIFIED="1353599363627" TEXT="Models can learn">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1353077980585" ID="ID_1304572896" MODIFIED="1353343566009" TEXT="Slow Learning">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1353078744330" ID="ID_1640175208" MODIFIED="1353343566011">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Risc of Catastrophic
    </p>
    <p>
      Interferance
    </p>
  </body>
</html></richcontent>
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1353079184442" ID="ID_375164592" MODIFIED="1353343566015">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Can be dealt with
    </p>
    <p>
      diferent mechanisms
    </p>
  </body>
</html></richcontent>
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1353079252418" ID="ID_170971763" MODIFIED="1353343566016" TEXT="LTM (NeoCortex)">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1353079266185" ID="ID_457634700" MODIFIED="1353343566016" TEXT="STM ( Hippocampus)">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1353080266370" ID="ID_940065369" MODIFIED="1353514975129" TEXT="Botvinick et Plaut 2004">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1353001310607" ID="ID_676231866" MODIFIED="1353343566018">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Some claim connectionist approaches
    </p>
    <p>
      are similar to chaining mechanisms
    </p>
  </body>
</html></richcontent>
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1353514890218" ID="ID_332893472" MODIFIED="1353599364499" TEXT="Models">
<node COLOR="#111111" CREATED="1353076553777" ID="ID_344425051" MODIFIED="1353343566019" TEXT="Simple Recurrent Neural Networks (SRN)">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1353078910497" ID="ID_1159332882" MODIFIED="1353343566019" TEXT="Time Dependent">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1353078927993" ID="ID_301195923" MODIFIED="1353514863743">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Each state depends
    </p>
    <p>
      on the precceding one
    </p>
  </body>
</html></richcontent>
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1353076520052" ID="ID_4502855" MODIFIED="1353514993485" TEXT="Elman 1990">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1353077822601" ID="ID_1880212316" MODIFIED="1353514987446" TEXT="Botvinick et Plaut 2004">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1353077813985" ID="ID_835973996" MODIFIED="1353514991710" TEXT="Botvinick et Plaut 2002">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1353078703817" ID="ID_735424796" MODIFIED="1353599338592" TEXT="Distributed Representations">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
</node>
<node CREATED="1353514900170" ID="ID_1577938847" MODIFIED="1354099712793" TEXT="Phonological Loops">
<node CREATED="1353514910466" ID="ID_105605448" MODIFIED="1353514937928" TEXT="Burgess et Hitch 1999">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1353515031378" ID="ID_207167126" MODIFIED="1353515047619" TEXT="Competitive Queing"/>
<node CREATED="1353599327160" ID="ID_423938561" MODIFIED="1353599336224" TEXT="Localist Representations"/>
<node CREATED="1354099812705" ID="ID_1674845214" MODIFIED="1354099836146" TEXT="Shows">
<node CREATED="1354099837776" ID="ID_242679672" MODIFIED="1354099841297" TEXT="Recency"/>
<node CREATED="1354099846368" ID="ID_1461773450" MODIFIED="1354099849954" TEXT="Primacy"/>
<node CREATED="1354099850728" ID="ID_1263335758" MODIFIED="1354099854929" TEXT="Modality"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1353001425440" ID="ID_1482505386" MODIFIED="1353514969938" TEXT="Botvinick et Plaut 2004">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1353079057634" ID="ID_546427191" MODIFIED="1353343566023" TEXT="Elman1990">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1352995609679" ID="ID_239739406" MODIFIED="1353343566023" TEXT="Hierarchical">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1353001180992" ID="ID_1831514555" MODIFIED="1353343566023" TEXT="Cannot Incorporate Learning ">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1353079317057" ID="ID_504302768" MODIFIED="1353514964699" TEXT="Botvinick et Plaut 2004)">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1352995763599" ID="ID_1541281537" MODIFIED="1353343566024" TEXT="Schemas">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1353078687841" ID="ID_495139556" MODIFIED="1353343566024" TEXT="Localist Representations">
<edge COLOR="#111111" WIDTH="thin"/>
</node>
<node COLOR="#111111" CREATED="1353079024210" ID="ID_349293760" MODIFIED="1353343566024" TEXT="Main Example">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1353001444663" ID="ID_58598339" MODIFIED="1353514958001" TEXT="Cooper et Shallice 2000">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1353078089433" ID="ID_556662600" MODIFIED="1353343566025" TEXT="Reinforcement Learning">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1353078097650" ID="ID_192789583" MODIFIED="1353343566025" TEXT="O&apos;Reilly2006">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1353078667162" ID="ID_1078570168" MODIFIED="1353502086010" POSITION="right" TEXT="Time">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1353079437553" ID="ID_865376181" MODIFIED="1353343566026" TEXT="Rythmic ">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1353079453185" ID="ID_1618677774" MODIFIED="1353343566026" TEXT="Lashley1951">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1353426107078" ID="ID_1946637231" MODIFIED="1353587144583" TEXT="Dependent">
<icon BUILTIN="help"/>
<node CREATED="1353348073441" ID="ID_708685734" MODIFIED="1354620758684" TEXT="Oscillator based model (OSCAR)">
<node CREATED="1353347958498" ID="ID_980001831" MODIFIED="1354620529420" TEXT="Brown et al 2000">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1353510728282" ID="ID_616615327" MODIFIED="1353510736884" TEXT="Phonological Loop">
<node CREATED="1353426154757" ID="ID_1430208110" MODIFIED="1353587340959" TEXT="Burgess et Hitch 1999">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1353426109662" ID="ID_1086310487" MODIFIED="1353587153095" TEXT="Independent">
<icon BUILTIN="help"/>
<node CREATED="1353501480549" ID="ID_1219326796" MODIFIED="1353501608205" TEXT="Supported on Empirical data">
<node CREATED="1353426128509" ID="ID_1937985358" MODIFIED="1353426141555" TEXT="Lewandowsky2006">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1353501590602" ID="ID_932803570" MODIFIED="1353501595432" TEXT="Event-based"/>
</node>
</node>
<node COLOR="#111111" CREATED="1353079635442" ID="ID_854677808" MODIFIED="1357556920273" POSITION="left" TEXT="Hierarchical in Nature">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1353079960649" ID="ID_421513036" MODIFIED="1353343566027" TEXT="Initially contradicted">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1353080005154" ID="ID_1488964758" MODIFIED="1353343566027" TEXT="Later acknowledged">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1353080018801" ID="ID_823348863" MODIFIED="1353515001742" TEXT="Botvinick 2008">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node COLOR="#111111" CREATED="1353080040706" ID="ID_189941680" MODIFIED="1353515006878" TEXT=" Botvnick et Plaut 2004">
<edge COLOR="#111111" WIDTH="thin"/>
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1354620504586" ID="ID_470886872" MODIFIED="1354620522815" TEXT="Brown et al 2000">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1354620544074" ID="ID_657699488" MODIFIED="1354620565152" TEXT="Cooper et Shallice 2000">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1354620552354" ID="ID_1424279259" MODIFIED="1354620563279" TEXT="Lashley 1951">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1357556921314" ID="ID_1989802806" MODIFIED="1357556960164" TEXT="Based on empirical observation">
<node CREATED="1357556963481" ID="ID_599635275" MODIFIED="1357556968413" TEXT="Common Sense"/>
<node CREATED="1357556968858" ID="ID_1283261853" MODIFIED="1357557014134" TEXT="Human Studies">
<node CREATED="1357556975338" ID="ID_1530933774" MODIFIED="1357556990598" TEXT="Hard et al 2006">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1353086515466" ID="ID_119436360" MODIFIED="1353502087228" POSITION="right" TEXT="Chunking">
<edge COLOR="#111111" WIDTH="thin"/>
<node CREATED="1353424714637" ID="ID_108456775" MODIFIED="1353587250262" TEXT="independent on time differentiation">
<icon BUILTIN="help"/>
<node CREATED="1353424721581" ID="ID_170812707" MODIFIED="1353424739026" TEXT="Frankish1989">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1353425004726" ID="ID_644557717" MODIFIED="1353425462915" TEXT="Frankish1995">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1353425012741" ID="ID_1680052223" MODIFIED="1353425462915" TEXT="Reeves2000">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1353425591125" ID="ID_1348652773" MODIFIED="1353501513840">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Faster recalls for first
    </p>
    <p>
      and last elements within a chunk
    </p>
  </body>
</html></richcontent>
<node CREATED="1353425662277" ID="ID_103964456" MODIFIED="1353425686363" TEXT="Lewandowsky2006">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1353501669185" ID="ID_1244372923" MODIFIED="1353501692751" TEXT="Henson1999a">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1353501826673" ID="ID_1189344396" MODIFIED="1353501832398" TEXT="Henson1999b">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
<node CREATED="1353501681602" ID="ID_1643071825" MODIFIED="1353501692751" TEXT="Hitch1996">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
<node CREATED="1353587285607" ID="ID_126129355" MODIFIED="1353587301232" TEXT="May use time differentiation">
<node CREATED="1353587309102" ID="ID_1985118097" MODIFIED="1353587336220" TEXT="Burgess et Hitch 1999">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1352995982199" ID="ID_1087031844" MODIFIED="1354293195586" POSITION="right" TEXT="Empirical Observation">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1352995483944" ID="ID_1198269988" MODIFIED="1353511737954" TEXT="Errors">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
<node COLOR="#111111" CREATED="1353078279713" ID="ID_880335372" MODIFIED="1353343565988" TEXT="Common Errors">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1352995496711" ID="ID_1541163536" MODIFIED="1353343565988" TEXT="Capture/Action Slips">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1354619989106" ID="ID_1062893070" MODIFIED="1354620024083">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Recall&#160;&#160;a list and at some
    </p>
    <p>
      point switch to another list
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node COLOR="#111111" CREATED="1352995520015" ID="ID_457239986" MODIFIED="1353343565988" TEXT="Omission">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1354619931196" ID="ID_1686846243" MODIFIED="1354619947301" TEXT="Forgetting of an item in a list"/>
</node>
<node COLOR="#111111" CREATED="1352995547039" ID="ID_1090628472" MODIFIED="1354620138643" TEXT="Perserveration/Repetition">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1354620142993" ID="ID_982158905" MODIFIED="1354620159381" TEXT="Wrongly recall the same item twice"/>
<node CREATED="1354620166674" ID="ID_1701573005" MODIFIED="1354620180861" TEXT="Rare error">
<node CREATED="1354620183330" ID="ID_1141148178" MODIFIED="1354620193887" TEXT="Henson et al 1996">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node COLOR="#111111" CREATED="1352995557415" ID="ID_583807666" MODIFIED="1353343565989" TEXT="Obj. Substitution">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1352995534775" ID="ID_74784997" MODIFIED="1353343565989" TEXT="Anticipation">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
<node CREATED="1354619956682" ID="ID_1985926582" MODIFIED="1354619974021" TEXT="Recall an item too soon on a list"/>
</node>
<node CREATED="1353598978800" ID="ID_123579559" MODIFIED="1353598985671" TEXT="Intrusion">
<node CREATED="1353598986223" ID="ID_1911223109" MODIFIED="1353598998499" TEXT="Recall of an item external to the sequence"/>
</node>
</node>
<node COLOR="#111111" CREATED="1353078289522" ID="ID_1837674578" MODIFIED="1353512680104" TEXT="Behavior Disorder">
<edge COLOR="#111111" WIDTH="thin"/>
<node COLOR="#111111" CREATED="1353076785506" ID="ID_1197748728" MODIFIED="1353343565990" TEXT="Action Disorganization Syndrome (ADS)">
<edge COLOR="#111111" WIDTH="thin"/>
<font NAME="SansSerif" SIZE="12"/>
</node>
<node COLOR="#111111" CREATED="1353343424161" ID="ID_408224813" MODIFIED="1353343565990" TEXT="Aphasia">
<edge COLOR="#111111" WIDTH="thin"/>
<node CREATED="1353512682754" ID="ID_1204181806" MODIFIED="1353512688578" TEXT="Language disorder"/>
</node>
</node>
</node>
<node CREATED="1353511754618" ID="ID_1443403882" MODIFIED="1353511762952" TEXT="Recency">
<node CREATED="1353587053016" ID="ID_792821762" MODIFIED="1353587090316">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      recent events are more easily
    </p>
    <p>
      remembered than older ones
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1353511763250" ID="ID_1492854405" MODIFIED="1353511767810" TEXT="Primacy">
<node CREATED="1354099144707" ID="ID_1431086254" MODIFIED="1354099172296" TEXT="First elements of a sequence or sub-sequence are more easily remembered"/>
</node>
<node CREATED="1353594739375" ID="ID_1120175128" MODIFIED="1353594744864" TEXT="Modality">
<node CREATED="1353594746655" ID="ID_1560160452" MODIFIED="1353594800536">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      The way as inputs are given
    </p>
    <p>
      &#160;influences memorization and recall
    </p>
  </body>
</html></richcontent>
</node>
</node>
<node CREATED="1354203416590" FOLDED="true" ID="ID_859197987" MODIFIED="1377101752484">
<richcontent TYPE="NODE"><html>
  <head>
    
  </head>
  <body>
    <p>
      Dependence
    </p>
    <p>
      on list length
    </p>
  </body>
</html></richcontent>
<node CREATED="1354203424133" ID="ID_1390054239" MODIFIED="1354203436000" TEXT="Longer lists are more hardly remembered"/>
</node>
<node CREATED="1357557020930" ID="ID_500916214" MODIFIED="1357557028629" TEXT="Hierarchical">
<node CREATED="1357557033665" ID="ID_399858432" MODIFIED="1357557043111" TEXT="Hard et al 2006">
<font BOLD="true" NAME="SansSerif" SIZE="12"/>
</node>
</node>
</node>
<node CREATED="1354620476041" ID="ID_267812670" MODIFIED="1354620479261" POSITION="left" TEXT="Robotics"/>
</node>
</map>
