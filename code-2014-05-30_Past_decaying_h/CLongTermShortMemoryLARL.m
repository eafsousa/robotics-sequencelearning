classdef CLongTermShortMemoryLARL < handle
    %CLongTermMemory Implementation the long term memory model developed by
    %Emanuel Sousa on the Scope of the research work made during the PhD
    %program.
    %   Detailed explanation goes here
    
    properties (SetAccess = public, GetAccess = public)
        x0 = 1;
        dx = 1;
        xmax = 100;
        x
        
        kernel_Pa = zeros(1,100);
        kernel_Pr = zeros(1,100);
        
        h_past = zeros(1,100);
        h_present = zeros(1,100);
        
        u_past
        u_present
        
        sigma_past1 = 1;
        sigma_past2 = 2;
        sigma_present = 1;
        
        kernelAmpPa1 = 2;
        kernelAmpPa2 = 1;
        kernelAmpPr = 2;
        
        kernelInibPa = 0;
        kernelInibPr = 0;
        
        G_vi_pa = 0;
        G_vi_pr = 0;
        G_pa2pr = 0;
        G_pr2pa = 0;
        G_recall = 0;
        G_STM_pr = 0;
        G_vi_STM = 0;
        G_HighLevInp = 0;
        
        h_past_TAU = 10;
        h_past_ref_learning = 1;
        h_past_ref_recall = 1;
        h_present_ref = 1;
        learning_TAU = 10;
        
        u_past_TAU = 10;
        u_present_TAU = 10;
        
        u_hcml_tau = 1.5;
        u_ecl_tau = 1.5;
        
        A_past2pres
        A_pres2past
        A
        
        A_high2low
        A_low2high
        
        dA
        
        h_past_low
        
        h_pastMode = 0;
        
        lambda
        beta
        eta = 0;
        
        index_k = 1;
        
        % U_HCML-------------------------------
        h_HCML = zeros(1,100);
        u_HCML = zeros(1,100);
        s_HCML = zeros(1,100);
        s_HCML_err = zeros(1,100);
        s_HCML_inp = zeros(1,100);
        
        % u_TML--------------------------------
        h_TML = zeros(1,100);
        u_TML = zeros(1,100);
        %s_TML_VerbInp = zeros(1,100);
        s_TML = zeros(1,100);
        
        inhibInp2HCML  = zeros(1,100);
        
        lambda_list = [5 4 3 2 1];
        lambda_level = 1;
        
        u_STM
        s_STM
        h_STM
        
        u_STM_tau = 20; % time constant of dynamic field u_SM

        k_h_STM
        
        TotalVariation = zeros(1,50000);
        
    end
    properties (SetAccess = private, GetAccess = public)
        flag_rebuild_arch = 0;
        flag_reset_past = 0;
        useHCML = false;
        l_kernel
        l_kernelpr
        Namp
        visualInput
        recall
        presExcit
        pastInib
        
        field_length
        
        s_present
        s_past
        
        error
        mode = 0; %By default it is learning
        sizeHighLevelInput = 0;
        highlevelInput
        Ainit
        populationLength = 1;
        iter = 0;
        logIter = 0;
        
        %Logs
        log_s_past
        log_u_past
        log_s_present
        log_u_present
        log_highlevelInput
        log_visualInput
        log_recall
        log_error
        log_presExcit
        log_pastInib
        log_s_HCML
        log_s_TML
        log_u_HCML
        log_u_TML
        log_s_STM
        log_u_STM
        h_TML_high
        h_TML_low
        log_dt
        
        %Logs for the complete fields
        log_s_past_field
        log_u_past_field
        log_s_present_field
        log_u_present_field
        log_highlevelInput_field
        log_visualInput_field
        log_recall_field
        log_error_field
        log_presExcit_field
        log_pastInib_field
        log_s_HCML_field
        log_s_TML_field
        log_u_HCML_field
        log_u_TML_field
        log_s_STM_field
        log_u_STM_field
        fileMEm
        
        logPos
        errorPopu_num
        %Time series
        tsEvents = [];
        tsEventsIter = [];
        thisClassTic = [];
        freezed_upast = false;
        
        boxes_hnd;
        flag_editingParams = 0;
        
        flag_logFields = 0;
        % set up the interaction kernel
        past_Namp = 0;  
        K;
        
        fig
        hnd_var
    end
    
    methods
        %% Constructor
        % bla bla
        %
        function obj = CLongTermShortMemoryLARL ( varargin )
            obj.set(varargin{:});
        end
        
        %% Set Function
        function set(obj,varargin)
            %Definition of variables
            obj.flag_rebuild_arch = 0;
            num_argin = numel(varargin);
            for n = 1:2:num_argin
                arg = varargin{n};
                switch lower(arg)
                    %========================================
                    case 'x0'
                        obj.x0 = varargin{n+1};
                        obj.flag_rebuild_arch = 1;
                    case 'dx'
                        obj.dx = varargin{n+1};
                        obj.flag_rebuild_arch = 1;
                    case 'xmax'
                        obj.xmax = varargin{n+1};
                        obj.flag_rebuild_arch = 1;
                    case 'sigma_past1'
                        obj.sigma_past1 = varargin{n+1};
                        obj.flag_rebuild_arch = 1;
                    case 'sigma_past2'
                        obj.sigma_past2 = varargin{n+1};
                        obj.flag_rebuild_arch = 1;
                    case 'sigma_present'
                        obj.sigma_present = varargin{n+1};
                        obj.flag_rebuild_arch = 1;
                    case 'kernel_amp_pa1'
                        obj.kernelAmpPa1 = varargin{n+1};
                        obj.flag_rebuild_arch = 1;
                    case 'kernel_amp_pa2'
                        obj.kernelAmpPa2 = varargin{n+1};
                        obj.flag_rebuild_arch = 1;
                    case 'kernel_amp_pr'
                        obj.kernelAmpPr = varargin{n+1};
                        obj.flag_rebuild_arch = 1;
                    case 'kernel_inib_pa'
                        obj.kernelInibPa = varargin{n+1};
                        obj.flag_rebuild_arch = 1;
                    case 'kernel_inib_pr'
                        obj.kernelInibPr = varargin{n+1};
                        obj.flag_rebuild_arch = 1;
                    case 'g_vi_pa'
                        obj.G_vi_pa = varargin{n+1};
                    case 'g_vi_pr'
                        obj.G_vi_pr = varargin{n+1};
                    case 'g_pa2pr'
                        obj.G_pa2pr = varargin{n+1};
                    case 'g_pr2pa'
                        obj.G_pr2pa = varargin{n+1};
                    case 'g_recall'
                        obj.G_recall = varargin{n+1};
                    case 'g_stm_pre'
                        obj.G_STM_pr = varargin{n+1};
                    case 'g_highlevinp'
                        obj.G_HighLevInp = varargin{n+1};
                    case 'h_past_tau'
                        obj.h_past_TAU = varargin{n+1};
                    case 'learning_tau'
                        obj.learning_TAU = varargin{n+1};
                    case 'h_past_ref_learning'
                        obj.h_past_ref_learning = varargin{n+1};
                    case 'h_past_ref_recall'
                        obj.h_past_ref_recall = varargin{n+1};
                    case 'h_present_ref',
                        obj.h_present_ref = varargin{n+1};
                        obj.h_present = ones(1,obj.xmax)*obj.h_present_ref;
                    case 'h_past_low'
                        obj.h_past_low = varargin{n+1};
                    case 'noise_amplitude'
                        obj.Namp = varargin{n+1};
                        if isa(obj.u_present,'CDynamicNeuralField')
                            obj.u_present.set('Namp',obj.Namp);
                        end
                   case 'past_noise_amplitude'
                        obj.past_Namp = varargin{n+1};
                        if isa(obj.u_past,'CDynamicNeuralField')
                            obj.u_past.set('Namp',obj.past_Namp);
                        end     
                    case 'lambda'
                        obj.lambda = varargin{n+1};
                    case 'lambda_list'
                        obj.lambda_list = varargin{n+1};
                    case 'beta'
                        obj.beta = varargin{n+1};
                    case 'matr_init_val'
                        a = size(varargin{n+1});
                        if (a(1)==obj.xmax+obj.sizeHighLevelInput) && a(2)==obj.xmax
                            obj.Ainit = varargin{n+1};
                            obj.A = obj.Ainit;
                        end
                    case 'u_past_tau'
                        obj.u_past_TAU = varargin{n+1};
                    case 'u_present_tau'
                        obj.u_present_TAU = varargin{n+1};
                    case 'sizehighlevelinput'
                        obj.sizeHighLevelInput = varargin{n+1};
                    case 'eta'
                        obj.eta = varargin{n+1};
                    case 'populationlength'
                        obj.populationLength = varargin{n+1};
                    case 'usehcml'
                        obj.useHCML = varargin{n+1};
                        obj.flag_rebuild_arch = 1;
                    case 'u_hcml_tau'
                        obj.u_hcml_tau = varargin{n+1};
                    case 'u_tml_tau'
                        obj.u_ecl_tau = varargin{n+1};
                    case 'g_vi_stm'
                        obj.G_vi_STM = varargin{n+1};
                    case 'k_h_stm'
                        obj.k_h_STM = varargin{n+1};
                    case 'u_stm_tau'
                        obj.u_STM_tau = varargin{n+1};
                    case 'h_tml_high'
                        obj.h_TML_high = varargin{n+1};
                    case 'h_tml_low'
                        obj.h_TML_low = varargin{n+1};
                    case 'h_tau_decay_list'
                        obj.K = varargin{n+1};
                end
            end
            % If there is a change on the global parameters:
            if obj.flag_rebuild_arch
                obj.x = obj.x0:obj.dx:obj.xmax;
                obj.field_length = length(obj.x);
                
                %% Kernel Mexican Hat==============================================
                obj.kernel_Pa = EGauss(obj.kernelAmpPa1,obj.x,obj.xmax/2,obj.sigma_past1,0.0)...
                    - EGauss(obj.kernelAmpPa2,obj.x,obj.xmax/2,obj.sigma_past2, 0.0);
                %% Kernel Gauss====================================================
                obj.kernel_Pr = EGauss(obj.kernelAmpPr,obj.x,obj.xmax/2,obj.sigma_present,obj.kernelInibPr);
                
                obj.l_kernel = FRamp(EGauss(1,obj.x,obj.xmax/2,2.5,0.1));
                obj.l_kernelpr = FRamp(EGauss(1,obj.x,obj.xmax/2,1.5,0.1));

                
                obj.h_past = obj.h_past_ref_learning*ones(1,obj.xmax);
                
                %Initialize Vectors
                obj.s_past = zeros(1,obj.xmax);
                obj.s_present = zeros(1,obj.xmax);
                obj.highlevelInput = zeros(1,obj.xmax);
                obj.visualInput = zeros(1,obj.xmax);
                obj.recall = zeros(1,obj.xmax);
                obj.error = zeros(1,obj.xmax);
                
                obj.presExcit = zeros(1,obj.xmax);
                obj.pastInib = zeros(1,obj.xmax);
                obj.recall = zeros(1,obj.xmax);
                
                %Create the DNFs
                obj.u_past = CDynamicNeuralField(obj.dx,obj.h_past,...
                    obj.kernel_Pa,obj.u_past_TAU,obj.past_Namp,0 );
                
                obj.u_present = CDynamicNeuralField(obj.dx,obj.h_present,...
                    obj.kernel_Pr,obj.u_present_TAU,obj.Namp,0 );
                
                obj.u_present.set('low_thres',-4);%-1000
                %Builds the inhibitory matrix from past to present
                am = diag(ones(1,obj.xmax));
                
                for i=1:obj.xmax
                    bm = conv(am(i,:),ones(1,11));
                    am(i,:) = bm(6:end-5);
                end
                
                obj.A_past2pres = -1*obj.G_pa2pr * am;
                
                %Builds the excitatory matrix from present to past
                am = diag(ones(1,obj.xmax));
                
                for i=1:obj.xmax
                    bm = conv(am(i,:),ones(1,5));
                    am(i,:) = bm(3:end-2);
                end
                obj.A_pres2past = obj.G_pr2pa * diag(ones(1,obj.xmax));
                
                
                
                
                
                %If necessary defines initial values of the matrix
                if obj.Ainit
                    obj.A = obj.Ainit;
                else
                    obj.A = zeros(obj.sizeHighLevelInput+obj.xmax,obj.xmax);
                end
                
                % TML =========================
                kern_TML = EGauss(1.5,obj.x,obj.field_length/2,obj.sigma_present/2,0.5);
                obj.h_TML = obj.h_TML_low*ones(1,obj.field_length);
                obj.u_TML = CDynamicNeuralField(obj.dx, obj.h_TML , kern_TML ,obj.u_ecl_tau, 0, 0 );
                %obj.s_TML_VerbInp = zeros(1,obj.field_length);
                obj.s_TML = zeros(1,obj.field_length);
                
                obj.TotalVariation = zeros(1,obj.xmax);
                %% case the user wants to use the HCML
%                 if obj.useHCML
%                     
%                     
%                     
%                     
%                     % u_HCML
%                     kern_HCML = 0.5*(EGauss(1.0,obj.x,obj.field_length/2,obj.sigma_present,0)-EGauss(0.5,obj.x,obj.field_length/2,obj.sigma_present*2,0));
%                     obj.h_HCML = -2.0*ones(1,obj.field_length);
%                     obj.u_HCML = CDynamicNeuralField(obj.dx, obj.h_HCML , kern_HCML ,obj.u_hcml_tau, 0, 2);
%                     obj.s_HCML = zeros(1,obj.field_length);
%                     obj.s_HCML_err = zeros(1,obj.field_length);
%                     obj.s_HCML_inp = zeros(1,obj.field_length);
%                     obj.inhibInp2HCML = zeros(1,obj.field_length);
%                     
% 
%                     
%                     % High level to low level neural net
%                     obj.A_high2low = CNeuralNet(obj.field_length,...
%                         obj.field_length,...
%                         zeros(obj.field_length,obj.field_length),...
%                         1,1 );
%                     obj.A_high2low.TAU = 8;
%                     
%                     % High level to low level neural net
%                     obj.A_low2high = CNeuralNet(obj.field_length,...
%                         obj.field_length,...
%                         zeros(obj.field_length,obj.field_length),...
%                         1,1 );
%                     obj.A_low2high.TAU = 8;
%                     
%                 end
                
                %halfField = obj.field_length/2-0.5;
        
                
                % set up the interaction kernel
                %Ak=2;
                %b=0.3*2;
                %alpha=0.6*2;
                
                
                %% Kernel Mexican Hat==============================================
                kernel_STM = EGauss(2.5,obj.x,obj.xmax/2,3.0,0.0)...
                    - EGauss(1,obj.x,obj.xmax/2,5, 0.0);
                
                %kernel_STM2=Ak*exp(-b*abs(-halfField:halfField)).*(b*sin(abs(alpha*(-halfField:halfField)))+cos(alpha*(-halfField:halfField)));

                obj.h_STM = -4 * ones(1,obj.xmax);
                
                obj.u_STM = CDynamicNeuralField(obj.dx, obj.h_STM , kernel_STM ,obj.u_STM_tau, 0, 0 );
               
                obj.s_STM = zeros(1,obj.xmax);
        
            end
        end
        
        function showVariation(obj)
            obj.fig = figure;
            obj.hnd_var = plot(obj.fig,zeros(1,obj.xmax));
        end
        
        function updateVariationPlot(obj)
            set(obj.hnd_var,'ydata',obj.TotalVariation);
            set(obj.hnd_var,'xdata',1:obj.xmax);

        end
        
        function setMode(obj,mode)
            %Sets the working mode as learning/recall
            if strcmp(mode,'demonstration')
                obj.u_past.reset();
                obj.u_present.set('low_thres',-4);%-1000
                obj.u_present.reset();
                %puts the h back in the initial position and begins
                obj.h_STM = - 4 * ones(1,obj.xmax);
                obj.u_STM.u_init = obj.h_STM;
                obj.u_STM.reset();
                obj.mode = 2;
                obj.h_pastMode = 0;
                if(obj.iter)
                    obj.tsEvents = [obj.tsEvents,0];
                end
                
            elseif strcmp(mode,'rehearsal')
                obj.u_present.set('low_thres',-4);
                obj.u_past.reset();
                obj.u_present.reset();
                obj.mode = 0;
                obj.h_pastMode = 0;
                if(obj.iter)
                    obj.tsEvents = [obj.tsEvents,1];
                end
            elseif strcmp(mode,'recall')
                obj.u_past.reset();
                obj.u_present.set('low_thres',-4);
                obj.mode = 1;
                obj.h_pastMode = 1;
                if(obj.iter)
                    obj.tsEvents = [obj.tsEvents,1];
                end
            end
            if(obj.iter)
                obj.tsEventsIter=[obj.tsEventsIter,obj.iter];
            end
        end
        
        function resetPast(obj)
            obj.flag_reset_past = 1;
        end
        
        function freezePast(obj,flag)
            if flag
                obj.freezed_upast = true;
            else
                obj.freezed_upast = false;
            end
        end
        
        function setSTM(obj,stm)
            obj.u_STM.u_init = stm;
            obj.u_STM.reset();
        end
        
        function iterate(obj,visualInput,highlevelInput,lowLevelInib,dt)
            obj.iter = obj.iter + 1; 
            %Iteration function - Executes the algorithm
            
            %Propagates the present DField to the past through the connections
            obj.presExcit = FStep(obj.u_present.u - obj.beta) * obj.A_pres2past;
            
            %Builds the input of the CSG_PAST vector by propagating the CSG_present
            obj.s_past = obj.G_vi_pa * visualInput + obj.presExcit + lowLevelInib;
            
            
            
            %% BUILD PRESENT INPUT=============================================
            
            %Inhibitory contribution from past in present
            obj.pastInib = FStep(obj.u_past.u)*obj.A_past2pres;
            
            %Recall contribution
            obj.recall = obj.G_recall * FStep(obj.u_past.u)*obj.A;
            
            %visual input 
            obj.visualInput = visualInput;
            
            %If the mode is rehearsal
            k = double(obj.mode == 0 );
            
            %Builds the present as the sum of the components
            obj.s_present = obj.G_vi_pr * visualInput + ...
                            (k*obj.G_STM_pr + 0.00) * FRamp(obj.u_STM.u) +...
                            obj.pastInib + ...
                            + FRamp( obj.recall ) + ...
                            obj.G_HighLevInp*highlevelInput;
            
            if obj.flag_reset_past && all(obj.u_past.u<=0)
                obj.flag_reset_past = 0;
                obj.u_present.reset();
            end
            
            %% Differential dynamics for h addaptation of u_past================

            
            h0 = (1-obj.h_pastMode) * obj.h_past_ref_learning*ones(1,obj.xmax) + ...
                (obj.h_pastMode) * obj.h_past_ref_recall*ones(1,obj.xmax) + ...
                obj.flag_reset_past * -10*(obj.u_past.u>0);
            
            k2 = obj.K(obj.index_k);
            
            if ~obj.flag_reset_past
                dh_past = (1-FStep(obj.u_past.u)).*( h0 - obj.h_past) + FXX1(obj.u_past.u)*k2.*(obj.h_past_low - obj.h_past);
            else
                dh_past = h0 - obj.h_past;
            end

            
            obj.h_past = obj.h_past + dh_past/obj.h_past_TAU;
            
            %% ITERATE FIELDS==================================================
            if ~obj.freezed_upast
                obj.u_past.iterate(obj.s_past,obj.h_past,dt);
            end
            
            %iteration with limit in the lowest possible value of u_present
            obj.u_present.iterate2( obj.s_present ,obj.h_present,dt);
            
            %% NORMALIZATION===================================================
            
            %Past
            pastAboveThreshold = FRamp(obj.u_past.u - obj.lambda);
            
            if (any(pastAboveThreshold~=0)),
                aux = conv(FStep(obj.u_present.u),obj.l_kernel);
                pastAboveThreshold  = FRamp(pastAboveThreshold-10*FStep(aux(obj.xmax/2:3/2*obj.xmax-1)));
            end
            
            %Present
            presentAboveThreshold = FRamp(obj.u_present.u - obj.beta);
            
            if any(presentAboveThreshold~=0),
                var = conv((presentAboveThreshold),obj.l_kernelpr);
                %presentAboveThreshold = FStep(var(obj.xmax/2:3/2*obj.xmax-1));
                presentAboveThreshold = FXX1(2*var(obj.xmax/2:3/2*obj.xmax-1));
            end
            
            if any(presentAboveThreshold)
                
                obj.error = presentAboveThreshold - FStep(pastAboveThreshold)*obj.A;
                
                 obj.dA = repmat(presentAboveThreshold,obj.xmax+obj.sizeHighLevelInput,1).* (...
                     repmat(FStep(pastAboveThreshold)',1,obj.xmax).*(...
                     repmat(obj.error,obj.xmax+obj.sizeHighLevelInput,1)-obj.eta*obj.A)...
                     );
                
%                obj.dA = FStep(pastAboveThreshold)' * presentAboveThreshold*(...
%                    repmat(obj.error,obj.xmax+obj.sizeHighLevelInput,1)-obj.eta*obj.A);
                obj.TotalVariation = sum(obj.A);
                
                obj.A = obj.A + dt/obj.learning_TAU * obj.dA;
                obj.A(obj.A<0) = 0;
            end
            
            %If its on the demonstration
            if obj.mode
                
                %Builds input to TML and iterates it
                obj.s_TML = FRamp(obj.u_present.u);
                obj.u_TML.iterate( obj.s_TML,obj.h_TML,dt);
                
            end
            
            %if its on rehearsal mode
            if obj.mode == 2,
                obj.s_STM = obj.G_vi_STM * visualInput;
                obj.u_STM.iterate(obj.s_STM,obj.h_STM,dt);
                obj.h_STM = obj.h_STM+obj.k_h_STM*FStep(obj.u_STM.u);
            end
        end
        
        function trainInibNet(obj,dt)
            if obj.useHCML
                obj.A_low2high.learningInib(FStep(obj.u_past.u),FStep(obj.u_HCML.u+0.8),...
                    dt*0.07);
            end
        end
        
        function errorTrigger(obj)
            %obj.lambda_level = obj.lambda_level + 1;
%            obj.lambda = obj.lambda_list([obj.lambda_level]);

            % adjusts the index of the array containing the several values
            % of TAU_h_past which controls the decay rate of the h level in
            % u_past
            obj.index_k = obj.index_k + 1;

            obj.errorPopu_num = obj.getActivPopID();
            
            %Adjusts the h_level of the TML layer
            obj.h_TML = obj.h_TML_high*ones(1,obj.field_length);

            disp(['Error occurred. Decay rate k2 is now equal to ', num2str(obj.K(obj.index_k))]);

        end
        
        function errorCorrected(obj)
            obj.h_TML = obj.h_TML_low*ones(1,obj.field_length);
        end
        
        function resetError(obj)
            obj.s_HCML_err = zeros(1,obj.field_length);
            %obj.s_TML_VerbInp = zeros(1,obj.field_length);
        end
        
        function forceResetFields(obj,str)
            switch(str)
                case 'past'
                    obj.u_past.reset();
                case 'present'
                    obj.u_present.reset();
            end
        end
        
        function fi = getUpast(obj)
            fi = obj.u_past.outputField();
        end
        
        function fi = getUpres(obj)
            fi = obj.u_present.u;
        end
        
        function id = getActivPopID(obj)
            id = -1;
            vec = FRamp(obj.u_present.u);
            if any(vec~=0),
                [~,m] = max(vec);
                id = round((m-obj.populationLength/2)/obj.populationLength);
            end
            
        end
        
        function initializeLogTS(obj,pos)
            obj.logPos = pos;
            s = length(pos);
            
            obj.log_s_past = zeros(s,100000);
            obj.log_u_past = zeros(s,100000);
            obj.log_s_present = zeros(s,100000);
            obj.log_u_present = zeros(s,100000);
            obj.log_highlevelInput = zeros(s,100000);
            obj.log_visualInput = zeros(s,100000);
            obj.log_recall = zeros(s,100000);
            obj.log_error = zeros(s,100000);
            obj.log_presExcit = zeros(s,100000);
            obj.log_pastInib = zeros(s,10000);
            obj.log_s_TML = zeros(s,100000);
            obj.log_u_TML = zeros(s,100000);
            obj.log_s_STM = zeros(s,100000);
            obj.log_u_STM = zeros(s,100000);
            
            if obj.useHCML
                obj.log_s_HCML = zeros(s,100000);
                obj.log_s_TML = zeros(s,100000);
                obj.log_u_HCML = zeros(s,100000);
                obj.log_u_TML = zeros(s,100000);
            end
        end
        
        function initializeLogFields(obj)
            obj.log_s_past_field = CLoggerBin('filename','E:\Work\temp\log_s_past_field.bin');
            obj.log_s_past_field.openfile();
            
            obj.log_u_past_field = CLoggerBin('filename','E:\Work\temp\log_u_past_field.bin');
            obj.log_u_past_field.openfile();
            
            obj.log_s_present_field = CLoggerBin('filename','E:\Work\temp\log_s_present_field.bin');
            obj.log_s_present_field.openfile();
            
            obj.log_u_present_field = CLoggerBin('filename','E:\Work\temp\log_u_present_field.bin');
            obj.log_u_present_field.openfile();
            
            obj.log_s_STM_field = CLoggerBin('filename','E:\Work\temp\log_s_STM_field.bin');
            obj.log_s_STM_field.openfile();
            
            obj.log_u_STM_field = CLoggerBin('filename','E:\Work\temp\log_u_STM_field.bin');
            obj.log_u_STM_field.openfile();
            
            
            %    obj.log_s_past_field = zeros(obj.field_length,50000);
            %    obj.log_u_past_field = zeros(obj.field_length,50000);
            %    obj.log_s_present_field = zeros(obj.field_length,50000);
            %    obj.log_u_present_field = zeros(obj.field_length,50000);
             %   obj.log_s_STM_field = zeros(obj.field_length,50000);
             %   obj.log_u_STM_field = zeros(obj.field_length,50000);
            %            obj.log_highlevelInput_field = zeros(obj.field_length,50000);
%            obj.log_visualInput_field = zeros(obj.field_length,50000);
%            obj.log_recall_field = zeros(obj.field_length,50000);
%            obj.log_error_field = zeros(obj.field_length,50000);
%            obj.log_presExcit_field = zeros(obj.field_length,50000);
%            obj.log_pastInib_field = zeros(obj.field_length,50000);
%            obj.log_s_TML_field = zeros(obj.field_length,50000);
%            obj.log_s_TML_field = zeros(obj.field_length,50000);
           
%             if obj.useHCML
%                 obj.log_s_HCML_field = zeros(obj.field_length,50000);
%                 obj.log_s_TML_field = zeros(obj.field_length,50000);
%                 obj.log_u_HCML_field = zeros(obj.field_length,50000);
%                 obj.log_u_TML_field = zeros(obj.field_length,50000);
%             end
        end
        
        function startTic(obj)
            obj.thisClassTic = tic;
        end
        
        function logData(obj)
            obj.logIter = obj.logIter + 1;
            pos = obj.logPos;
            for i=1:10,
               [~,val] = max(obj.u_present.u(1,pos(i)-2:pos(i)+2));
               pos(i) = pos(i)-3+val;
            end
            
            obj.log_s_past(:,obj.logIter) = obj.s_past(1,obj.logPos)';
            obj.log_u_past(:,obj.logIter) = obj.u_past.u(1,obj.logPos)';
            obj.log_s_present(:,obj.logIter) = obj.s_present(1,pos)';
            obj.log_u_present(:,obj.logIter) = obj.u_present.u(1,pos)';
            obj.log_highlevelInput(:,obj.logIter) =  obj.highlevelInput(1,pos)';
            obj.log_visualInput(:,obj.logIter) = obj.visualInput(1,obj.logPos)';
            obj.log_recall(:,obj.logIter) = obj.recall(1,pos)';
            obj.log_error(:,obj.logIter) = obj.error(1,pos)';
            obj.log_presExcit(:,obj.logIter) = obj.presExcit(1,pos)';
            obj.log_pastInib(:,obj.logIter) = obj.pastInib(1,pos)';
            obj.log_s_TML(:,obj.logIter) = obj.s_TML(1,obj.logPos)';
            obj.log_u_TML(:,obj.logIter) = obj.u_TML.u(1,obj.logPos)';
            obj.log_s_STM(:,obj.logIter) = obj.s_STM(1,obj.logPos)';
            obj.log_u_STM(:,obj.logIter) = obj.u_STM.u(1,obj.logPos)';
            
            %If the option for using the High level layer is set
            if obj.useHCML
                
                posHCML = EGetPeaksLocation(obj.u_HCML.u);
                
                if posHCML>0,
                    obj.log_s_HCML(:,obj.logIter) = obj.s_HCML(1,posHCML)';
                    obj.log_u_HCML(:,obj.logIter) = obj.u_HCML.u(1,posHCML)';
                else
                    obj.log_s_HCML(:,obj.logIter) = nan;
                    bj.log_u_HCML(:,obj.logIter) = nan;
                end

            end
            
            if obj.flag_logFields
                obj.logFields();
            end
            %If its not the first iteration
            if obj.logIter~=1,
                obj.log_dt(obj.logIter) = toc(obj.thisClassTic);
            end
            obj.thisClassTic = tic;
        end
        
        function logFields(obj)
            obj.log_s_past_field.addNewSample(obj.s_past(1,:)');
            obj.log_u_past_field.addNewSample(obj.u_past.u(1,:)');
            obj.log_s_present_field.addNewSample(obj.s_present(1,:)');
            obj.log_u_present_field.addNewSample(obj.u_present.u(1,:)');
            obj.log_s_STM_field.addNewSample(obj.s_STM(1,:)');
            obj.log_u_STM_field.addNewSample(obj.u_STM.u(1,:)');
        end
        
        function storeFieldLogs(obj,dir)
            mkdir(dir);
            obj.log_s_past_field.storeFileIn([dir,'log_s_past_field.bin']);
            obj.log_u_past_field.storeFileIn([dir,'log_u_past_field.bin']);
            obj.log_s_present_field.storeFileIn([dir,'log_s_present_field.bin']);
            obj.log_u_present_field.storeFileIn([dir,'log_u_present_field.bin']);
            obj.log_s_STM_field.storeFileIn([dir,'log_s_STM_field.bin']);
            obj.log_u_STM_field.storeFileIn([dir,'log_u_STM_field.bin']);
        end
        
        function closeLogFiles(obj)
            obj.log_s_past_field.closefile();
            obj.log_u_past_field.closefile();
            obj.log_s_present_field.closefile();
            obj.log_u_present_field.closefile();
            obj.log_s_STM_field.closefile();
            obj.log_u_STM_field.closefile();
        end
            
        function ts = createTSerie(obj,arr,tsname,dataNames)
            
            %builds the time array
            vec = zeros(1,obj.iter);
            vec(1) = obj.log_dt(1);
            for i=2:obj.iter,
                vec(i) = vec(i-1)+obj.log_dt(i);
            end

            ts = timeseries(arr(:,1:obj.iter),vec, 'name', tsname);

            if ~strcmp(dataNames,'')
                ts.UserData.names = dataNames;
            end
            if ~isempty(obj.tsEvents)
                ts = addevent(ts,cellstr(num2str(obj.tsEvents')),num2cell(obj.tsEventsIter'));
            end
        end
        
        function tsc = getLogData(obj,names)
            tsc = tscollection;
            tsc = addts(tsc,obj.createTSerie(obj.log_s_past,'s_past',names));
            tsc = addts(tsc,obj.createTSerie(obj.log_u_past,'u_past',names));
            tsc = addts(tsc,obj.createTSerie(obj.log_s_present,'s_present',names));
            tsc = addts(tsc,obj.createTSerie(obj.log_u_present,'u_present',names));
            tsc = addts(tsc,obj.createTSerie(obj.log_highlevelInput,'highlevelInput',names));
            tsc = addts(tsc,obj.createTSerie(obj.log_visualInput,'visualInput',names));
            tsc = addts(tsc,obj.createTSerie(obj.log_recall,'recall',names));
            tsc = addts(tsc,obj.createTSerie(obj.log_s_TML,'s_TML',names));
            tsc = addts(tsc,obj.createTSerie(obj.log_u_TML,'u_TML',names));
            tsc = addts(tsc,obj.createTSerie(obj.log_s_STM,'s_STM',names));
            tsc = addts(tsc,obj.createTSerie(obj.log_u_STM,'u_STM',names));
            
            if obj.useHCML
                tsc = addts(tsc,obj.createTSerie(obj.log_s_HCML,'s_HCML',names));
                tsc = addts(tsc,obj.createTSerie(obj.log_u_HCML,'u_HCML',names));

            end
            
            tsc = addts(tsc,obj.createTSerie(obj.log_presExcit,'presExcit',names));
            tsc = addts(tsc,obj.createTSerie(obj.log_pastInib,'pastInib',names));
 
        end
        
%         function ob = clusterFieldLogsStruct(obj)
%             ob.log_s_past_field = obj.log_s_past_field(:,1:obj.logIter);
%             ob.log_u_past_field = obj.log_u_past_field(:,1:obj.logIter);
%             ob.log_s_present_field = obj.log_s_present_field(:,1:obj.logIter);
%             ob.log_u_present_field = obj.log_u_present_field(:,1:obj.logIter);
%             
%         end
        
        function tsc = clusterFieldLogs(obj)
            
            tsc = tscollection;
            tsc = addts(tsc,obj.createTSerie(obj.log_s_past_field(:,1:obj.logIter),'s_past',''));
            tsc = addts(tsc,obj.createTSerie(obj.log_u_past_field(:,1:obj.logIter),'u_past',''));
            tsc = addts(tsc,obj.createTSerie(obj.log_s_present_field(:,1:obj.logIter),'s_present',''));
            tsc = addts(tsc,obj.createTSerie(obj.log_u_present_field(:,1:obj.logIter),'u_present',''));
            tsc = addts(tsc,obj.createTSerie(obj.log_highlevelInput_field(:,1:obj.logIter),'highlevelInput',''));
            tsc = addts(tsc,obj.createTSerie(obj.log_visualInput_field(:,1:obj.logIter),'visualInput',''));
            tsc = addts(tsc,obj.createTSerie(obj.log_recall_field(:,1:obj.logIter),'recall',''));
            tsc = addts(tsc,obj.createTSerie(obj.log_error_field(:,1:obj.logIter),'error',''));
            tsc = addts(tsc,obj.createTSerie(obj.log_presExcit_field(:,1:obj.logIter),'presExcit',''));
            tsc = addts(tsc,obj.createTSerie(obj.log_pastInib_field(:,1:obj.logIter),'pastInib',''));
            tsc = addts(tsc,obj.createTSerie(obj.log_s_TML_field(:,1:obj.logIter),'s_TML',''));
            tsc = addts(tsc,obj.createTSerie(obj.log_u_TML_field(:,1:obj.logIter),'u_TML',''));

            %If the option for using the High level layer is set
            if obj.useHCML
                tsc = addts(tsc,obj.createTSerie(obj.log_s_HCML_field(:,1:obj.logIter),'s_HCML',''));
                tsc = addts(tsc,obj.createTSerie(obj.log_u_HCML_field(:,1:obj.logIter),'u_HCML',''));
            end
        end
        
        function setLearningThresholdsGUI(obj)
            obj.flag_editingParams = 1;
            h = figure('pos',[300 300 300 80],...
                'menubar','none',...
                'numbertitle','off',...
                'CloseRequestFcn',@obj.closeCallback);
            for i=1:5,
                uicontrol('parent',h,...
                    'style','slider',...
                    'units','normalized',...
                    'position',[0 (5-i)*0.2 0.8 0.2],...
                    'min',0,...
                    'max',10,...
                    'value',obj.lambda_list(1,i),...
                    'callback',{@obj.setLearningThresholdCallback,i});
                
                obj.boxes_hnd(end+1) = uicontrol('parent',h,...
                    'style','text',...
                    'units','normalized',...
                    'pos',[0.8 (5-i)*0.2 0.2 0.2],...
                    'string',num2str(obj.lambda_list(1,i)));
            end

        end
        
        function setDecaysGUI(obj)
            obj.flag_editingParams = 1;
            h = figure('pos',[300 300 300 80],...
                'menubar','none',...
                'numbertitle','off',...
                'CloseRequestFcn',@obj.closeCallback);
            for i=1:5,
                uicontrol('parent',h,...
                    'style','slider',...
                    'units','normalized',...
                    'position',[0 (5-i)*0.2 0.8 0.2],...
                    'min',0,...
                    'max',1,...
                    'value',obj.K(1,i),...
                    'callback',{@obj.setLearningDecaysRateCallback,i});
                
                obj.boxes_hnd(end+1) = uicontrol('parent',h,...
                    'style','text',...
                    'units','normalized',...
                    'pos',[0.8 (5-i)*0.2 0.2 0.2],...
                    'string',num2str(obj.K(1,i)));
            end

        end
        
        function startFieldsLog(obj)
           obj.flag_logFields = 1; 
        end
        
        function stopFieldsLog(obj)
           obj.flag_logFields = 0; 
        end
        
    end
    
    methods(Access = private)
        function setLearningThresholdCallback(obj,src,evn,id)
            obj.lambda_list(id) = get(src,'value');
            set(obj.boxes_hnd(id),'string',num2str(obj.lambda_list(id)));
            if id == obj.lambda_level
                obj.lambda = obj.lambda_list(id);
            end
        end
        
        function setLearningDecaysRateCallback(obj,src,evn,id)
            obj.K(id) = get(src,'value');
            set(obj.boxes_hnd(id),'string',num2str(obj.K(id)));
        end
        
                
                
        
        function closeCallback(obj,src,evn)
            obj.flag_editingParams = 0;
        end
    end
end

