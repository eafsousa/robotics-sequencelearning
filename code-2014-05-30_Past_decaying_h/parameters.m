%% Sizes===================================================================
p.x0 = 1;
p.dx = 1;
p.poplength = 30;
p.n_pops = 10;
p.size = p.poplength*p.n_pops;
p.G_TAU = 0.2;%10;
p.Namp = 3.5;%3.0 for the real stuff. 3.8 for the simulations

p.kernelGloInib = 2;%25;

%% U_PAST==================================================================
p.kernelAmpPa1 = 9.3;
p.kernelAmpPa2 = 6.09;

p.sigma_past1 = 2;
p.sigma_past2 = 3;
p.h_past = -2.0; %-18
p.G_vi_pa = 1.05;

p.u_TAU_past = p.G_TAU*10;%*15;%30;
p.h_past_learning = -1.0;%.5;
p.h_past_recall = -0.5;
p.h_TAU = 2.8;
p.h_past_low = -4.5;
p.pastNamp = 0.3;

%% U_PRESENT===============================================================
p.kernelAmpPr = 9.4;%28;
p.kernelInibPr = 8.8;
p.u_TAU_present = p.G_TAU*10;
p.sigma_present = 7.0; %4.0
p.G_vi_pr = 17;%200;
p.G_STM_pr = 0.9;%200;
p.h_present = -1.6;
p.G_OML2AEL = 10;


%% CONNECTIONS=============================================================
p.G_pa2pr = 6.0;%5.6;%20;
p.G_pr2pa = 5.0;%3.9;%3.9
p.G_recall = 2.2;%2.5;
p.G_HighLevInp = 0.3;
p.G_STMinput_Recall = 0.1;


%% LEARNING================================================================
p.d_TAU_pos = 5;
p.d_TAU_neg = -.8;
p.matrix_TAU = p.G_TAU *20;%15;%5.0;
p.eta = 0.1;
%Lab pc: 0.1


%% TML=============================================
p.h_tml_low = -8;
p.h_tml_high= -1;


%% THRESHOLDS=============================================================
p.beta =5;% 10;        %Present Learning Threshold

p.lambda = 5.3;     %past Learning Threshold
% 7.7 one to one
% 6.5 two to one   
% 5.8 three to one
% 5.3 four to one
p.lambdaVec = 6.4; %6.0


%% H decay rates
%List of decay rates for the h_level of the past layer. Lower decay rates
%mean slower decays, which equivalates to a wider time window of past
%events considered on the learning
%
%Past events in the window:
% One: 0.15
% Two: 0.11
% Three:0.04 ????
% Four:0.033
% Five:0.03
%p.h_tau_decay_list = [0.08,0.07,0.03,0.033,0.03];
p.h_tau_decay_list = [0.14,0.08,0.05,0.033,0.03]; %[0.15,0.08,0.06,0.033,0.03];



%% Simulation variables 

% Time between simulated sub-goals
p.timeBetweenEvents = 4;%3;

% Variability of time between executions
p.timervariability = 2;
p.tasksToExecute = 1;

%% Number of training demonstrations in internal rehearsal
p.trainingEpochs = 30;%50;


%% Iterations per Epoch
% Number of iteration during which a input pattern of STM is presented one
% internal rehearsal presentation
p.iterPerEpoch = 230;

p.G_vi_STM = 7;

p.k_h_STM=1/20;

p.u_STM_tau = p.G_TAU*15;

%Simulated sequences
p.sequences = [ 1 2 3 4 5;...
                 1 4 5 2 3;...
                 1 9 7 6 8];



%Flora
 p.labels = {'BP';'LW';'LN';'RW';'RN';'GC';'RC';'MC';'BC';'TP'};

 
 