% online simulator for Dynamic Neural Fields, 1-layer
clear all
close all
clc


handles.aros = CAros();
handles.aros.initiateRobotDevices('vision','real');
handles.aros.startCommunications('vision');

%%%%%%%%%%%%%%%%%%%%
% model parameters %
%%%%%%%%%%%%%%%%%%%%

fieldSize = 181; % must be odd
halfField = floor(fieldSize/2);

tau_SM = 20; % time constant of dynamic field u_SM

k_hSM=1/300; %the growth rate of hSM

hSM=-4;

q_u = 0; % noise levels


%%%%%%%%%%%%%%%%%%
% initialization %
%%%%%%%%%%%%%%%%%%

% create row vectors for field activities
u_SM = hSM*ones(1, fieldSize);
output_SM=zeros(1,fieldSize);

% index of the current position in the history matrices
iHistory = 1;


% set up the interaction kernel
A=2;
b=0.3;
alpha=0.6;
kernel_SM=A*exp(-b*abs(-halfField:halfField)).*(b*sin(abs(alpha*(-halfField:halfField)))+cos(alpha*(-halfField:halfField)));


% noise 
noise_u = q_u * randn(1, fieldSize);

stop = 0;


savedInput=zeros(1,180);

figButton = figure;
i=0;
%load savedInput.mat

v = zeros(1,10);
c = zeros(1,10);
g = zeros(1,10);
m = zeros(1,10);

while ~stop
    
    pause(0.01);
    i = i+1;
    ch = get(gcf,'CurrentCharacter');
    set(gcf,'CurrentCharacter','1');
    
    if ch =='s',
        stop = 1;
    end
    
    inp = handles.aros.visionSystem.getInsertedObjects();
    
    c =  c + inp;
    
    v = (v | (c>10));
       
    m = m+v;
    
    g = m < 30;
    
    %The visual input
    sigma=2;
    stim_visual = 8*(g(1)*v(1)*gauss(1:fieldSize, 15,sigma)+...
        g(6)*v(6)*gauss(1:fieldSize, 45,sigma)+...
        g(7)*v(7)*gauss(1:fieldSize, 75,sigma)+...
        g(8)*v(8)*gauss(1:fieldSize, 105,sigma)+...
        g(9)*v(9)*gauss(1:fieldSize, 135,sigma)+...
        g(10)*v(10)*gauss(1:fieldSize, 165,sigma));
    
    % calculation of field outputs and value of h
    
    for k=1:fieldSize
        if u_SM(k)< 0
            output_SM(k)=0;
        else
            output_SM(k)=1;
        end
    end
    
    
    hSM= hSM+k_hSM*output_SM;
    disp(max(hSM));
    % circular padding of outputs for convolution
    u_SM_padded = [output_SM(halfField+2:fieldSize),output_SM, output_SM(:, 1:halfField)];
    
    % get endogenous input to fields by convolving outputs with interaction kernels
    conv_SM= conv2(1, kernel_SM, u_SM_padded,'valid');
    
    % update sequence memory
    
    u_SM=u_SM+1/tau_SM*(-u_SM+stim_visual+conv_SM+hSM)+noise_u;
    
    tStoreMemory = 1:i; % store field activities at every time step
    
end

handles.aros.stopCommunications('all');

%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% visualization of results %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%

% view plots of all stored field activities iteratively
nStoredFields = i;


if 1
    figure;
    for i =nStoredFields
        
        %plot of activation in the Sequence Memory
        plot(0:fieldSize-1, u_SM, '-b', 0:fieldSize-1, hSM, '--r',0:fieldSize-1, zeros(1, fieldSize), ':k', 'LineWidth',2);
        set(gca,'XTick',0:20:fieldSize-1);
        xlabel('dimension x','fontsize',12)
        title('Sequence Memory','fontsize',14)
        
        drawnow;
        % pause(0.001);
    end
end


save [2013-05-29]SequenceMemory_Emanuel_2 u_SM;


