
Vision = CYarpCommunication('/Control/HLC');
Vision.connectToDevice('/aros/vision');

sCommand = 
sText = ' ';
sfdata = [];
suparam = [];
sErrorcode = 0;


%%%%%%%%%%%%%%%%%%%%
% model parameters %
%%%%%%%%%%%%%%%%%%%%

fieldSize = 181; % must be odd
halfField = floor(fieldSize/2);

tau_SM = 20; % time constant of dynamic field u_SM

k_hSM=1/100; %the growth rate of hSM

hSM=-4;

q_u = 0; % noise levels


%%%%%%%%%%%%%%%%%%%%%%%%%%
% simulation time course %
%%%%%%%%%%%%%%%%%%%%%%%%%%

nTrials =3;
tMax =[400 200 200];
Total=tMax(1)+tMax(2)+tMax(3);

tStoreFields = 1:tMax(1); % store field activities at every time step


% set times of stimulus presentation
tStimulusStart = [200 1 1];
tStimulusEnd =[300 100 100];



%%%%%%%%%%%%%%%%%%
% initialization %
%%%%%%%%%%%%%%%%%%

% create row vectors for field activities
u_SM = hSM*ones(1, fieldSize);
output_SM=zeros(1,fieldSize);
time=zeros(nTrials,Total);
treal=zeros(1,Total);

% create matrices to store field activities at different times
history_s = zeros(nTrials * length(tStoreFields), fieldSize);
history_SM = zeros(nTrials * length(tStoreFields), fieldSize);
history_hSM = zeros(nTrials * length(tStoreFields), fieldSize);
history_time = zeros(nTrials * length(tStoreFields),Total,nTrials);


% index of the current position in the history matrices
iHistory = 1;
itime=1;

% set up the interaction kernel
A=2;
b=0.15;
alpha=0.3;
kernel_SM=A*exp(-b*abs(-halfField:halfField)).*(b*sin(abs(alpha*(-halfField:halfField)))+cos(alpha*(-halfField:halfField)));


% noise 
 noise_u = q_u * randn(1, fieldSize);


%%%%%%%%%%%%%%
% simulation %
%%%%%%%%%%%%%%
stim_center=zeros(1,nTrials);
stim_center(1)= 80;
%[max1,stim_center(1)]=max(input);
stim_center(2)= 40;
stim_center(3)= 120;

stim_ampl=[8 8 8];

% loop over trials
tic
for i = 1 : nTrials
 
  % prepare matrix that holds the stimulus for each time step
  stimulus = zeros(tMax(i), fieldSize);
  
  tStoreFields = 1:tMax(i); % store field activities at every time step
  
  % example: create Gaussian input
  stimPos=stim_center(i);
  stimT =8*gauss(1:fieldSize, stimPos,4);
  
  

  % write the stimulus pattern into the stimulus matrix for all time steps
  % where it should be active
  for j = tStimulusStart(i): tStimulusEnd(i)
    stimulus(j, :) = stimT;
  end
  
  
   
   % loop over time steps
  for t = 1 : tMax(i)
    % calculation of field outputs and value of h
    
    for k=1:fieldSize
        if u_SM(k)< 0
            output_SM(k)=0;
        else
            output_SM(k)=1;
        end
    end


   hSM= hSM+k_hSM*output_SM;

    % circular padding of outputs for convolution
    u_SM_padded = [output_SM(halfField+2:fieldSize),output_SM, output_SM(:, 1:halfField)];
    
   % get endogenous input to fields by convolving outputs with interaction kernels
    conv_SM= conv2(1, kernel_SM, u_SM_padded,'valid');
    
    
    
        
    % update sequence memory
  
    u_SM=u_SM+1/tau_SM*(-u_SM+stimulus(t, :)+conv_SM+hSM)+noise_u;
   
   treal(1,itime+1)=toc;
   
   for n=1: nTrials
   time(n,itime)=u_SM(stim_center(n));
   end
   
    itime=itime+1;
    
    % store field activities at the selected time steps
    if any(tStoreFields == t)    
    history_s(iHistory, :) = stimulus(t, :);
   % history_s(iHistory, :) =stimT;
    history_SM(iHistory, :) = u_SM;
     history_hSM(iHistory,:)=hSM;
     for n=1: nTrials
     history_time(iHistory,:,n)=time(n,:);
     end
     iHistory = iHistory + 1;
    end
  end
end


%%%%%%%%%%%%%%%%%%%%%%%%%%%%
% visualization of results %
%%%%%%%%%%%%%%%%%%%%%%%%%%%%


% view plots of all stored field activities iteratively
nStoredFields = nTrials * length(tStoreFields);


if 1
  figure;
  for i =800
    
      
    subplot(2,1,1)
    %plot of activation in the Sequence Memory
    plot(0:fieldSize-1, history_SM(i, :), '-b', 0:fieldSize-1, history_hSM(i,:), '--r',0:fieldSize-1, zeros(1, fieldSize), ':k', 'LineWidth',2);
    set(gca,'XTick',0:20:fieldSize-1);   
    xlabel('dimension x','fontsize',12)
    title('Sequence Memory','fontsize',14)
    
    subplot(2,1,2)
 
    %plot of iteration time
  plot(1:Total,history_time(i,:,1),'r',1:Total,history_time(i,:,2),'g',1:Total,history_time(i,:,3),'m',...
     0:Total-1,zeros(1,Total),'k',200,(-10:0.2:20),...
     400,(-10:0.2:20),600,(-10:0.2:20),'LineWidth',2);
    ylabel('activation u_{SM}','fontsize',12)
   xlabel ('time','fontsize',12)
   title('Sequence Memory','fontsize',14)
   
     drawnow;
   % pause(0.001);
  end
end


save SequenceMemory u_SM;
save StimulusCenter stim_center;

